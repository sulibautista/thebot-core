"use strict";

module.exports = {
  name: 'testPlugin2',
  
  testDirtyCmd: {
    offline: {
      exec: function(ch, cmdOpts, callback){
        ch.newDirtyProp = true;
        ch.dirty();
        callback(null, {dirty: 'dirty'});
      }
    }
  }
};