"use strict";

module.exports = {
  name: 'testPlugin',
  
  testCmd: {
    offline: {
      before: function(ch, cmdOpts, callback){
        let ret = {testBefore: true};
        callback(null, ret);
      },
      exec: function(ch, cmdOpts, callback, ret){
        ret.testExecuted = true;
        ret.output = 'testPlugin.testCmd.exec';
        callback(null, ret);
      },
      after: function(ch, cmdOpts, callback, ret){
        ret.testAfter = true;
        callback(null, ret);
      }
    },
    
    online: function(ch, cmdOpts, callback){
      callback(null, {output: 'testPlugin.testCmd.online.exec' });
    }
  }
};