"use strict";

const
  sinon = require('sinon'),
  _ = require('lodash'),
  conf = require('thebot-conf'),
  log = conf.log(),
  redis = conf.redis(),
  db = require('thebot-db')(),
  Channel = db.Channel,
  User = db.User,
  ChannelUser = db.ChannelUser,
  joinQueue = require('thebot-joinqueue')(redis),
  PluginManager = require('../lib/plugin-manager.js'),
  CmdSvc = require('../lib/command-service.js');

describe('Command service', function(){
  let
    pluginManager = new PluginManager(),
    userName,
    channelName,
    svc,
    chFields,
    createOpts,
    resolver,
    channel,
    dbChannel;
  
  before(function(done){
    // Load test commands
    pluginManager.loadPlugins(__dirname + '/command-mocks', done);
  });
  
  beforeEach(function(done){
    svc = new CmdSvc(pluginManager);

    resolver = svc.resolvers.twitch;
    userName = _.uniqueId('test');
    channelName = _.uniqueId('#');
    chFields = { twitch: { name: channelName } };
    createOpts = { resolver: resolver };

    Channel.createInDatabaseAndSetUp(channelName, createOpts, createOpts, function(err, c){
      channel = c;
      dbChannel = c.dbEntity;
      done(err);
    });
  });

  installDbAndCacheClearer();
  
  describe('#execCommand', function(){
    let
      baseCmd,
      validTestCmd;

    beforeEach(function(){
      baseCmd=  {
        id: 'testCmd',
        channelService: 'twitch'
      };

      validTestCmd = _.extend({
        userName: userName,
        channelName: channelName
      }, baseCmd);
    });

    it('should fail to execute invalid commands', function(done){
      let invalidInput = [
        null,
        undefined,
        arguments,
        "",
        true,
        false,
        1337,
        "invalid",
        {},
        {user: 'test'},
        {id: 'invalid_cmd'},
        {channel: channelName},
        {user: 'test', id: 'invalid_cmd'},
        {user: 'test', channel: channelName},
        {id: 'invalid_cmd', channel: channelName}
      ];
      
      let afterDone = _.after(invalidInput.length, done);
      
      invalidInput.forEach(function(input){
        svc.execCommand(input, function(err){
          expect(err).to.have.property('message', 'BOT_CMD_INVALID');
          afterDone();
        });
      });
    });
    
    it('should fail to execute unknown commands', function(done){
      svc.execCommand(_.defaults({ id: 'unknown_command'}, validTestCmd), function(err){
        expect(err).to.have.property('message', 'BOT_CMD_INVALID');
        done();
      });
    });

    it('should execute a command with userName and channelName', function(done){
      svc.execCommand(_.extend({ userName: userName, channelName: channelName }, baseCmd), function(err, res){
        expect(err).to.not.exist;
        expect(res).to.have.property('testExecuted', true);
        done();
      });
    });

    it('should execute a command with userName and channelId', function(done){
      svc.execCommand(_.extend({ userName: userName, channelId: dbChannel._id }, baseCmd), function(err, res){
        expect(err).to.not.exist;
        expect(res).to.have.property('testExecuted', true);
        done();
      });
    });

    it('should execute a command with userId and channelName', function(done){
      User.createInDatabase(userName, createOpts, function(err, dbUser){
        expect(err).to.not.exist;
        svc.execCommand(_.extend({ userId: dbUser._id, channelName: channelName }, baseCmd), function(err, res){
          expect(err).to.not.exist;
          expect(res).to.have.property('testExecuted', true);
          done();
        });
      });
    });

    it('should execute a command with userId and channelId', function(done){
      User.createInDatabase(userName, createOpts, function(err, dbUser){
        expect(err).to.not.exist;
        svc.execCommand(_.extend({ userId: dbUser._id, channelId: dbChannel._id }, baseCmd), function(err, res){
          expect(err).to.not.exist;
          expect(res).to.have.property('testExecuted', true);
          done();
        });
      });
    });


    it('should route a command to its online handler when a channel is online', function(done){
      joinQueue.setJoinStatus(dbChannel._id.id, 'joined', function(err){
        expect(err).to.not.exist;

        svc.execCommand(validTestCmd, function(err, res){
          expect(res.output).to.equal('testPlugin.testCmd.online.exec');
          joinQueue.delJoinStatus(dbChannel._id.id, done);
        });
      });
    });
    
    it('should cache the channel, user and channelUser in redis after a successful command', function(done){
      User.createInDatabase(userName, createOpts, function(err, dbUser){
        expect(err).to.be.null;

        let channelUserId = {
          user: dbUser._id,
          channel: dbChannel._id
        };

        svc.execCommand(validTestCmd, function(err){
          expect(err).to.be.null;

          let d = _.after(3, done);

          Channel.cacheGet(redis, dbChannel._id, function(err, res){
            expect(err).to.be.null;
            expect(res).to.exist;
            expect(JSON.parse(res)).to.have.deep.property('twitch.name', channelName);
            d();
          });

          User.cacheGet(redis, dbUser._id, function(err, res){
            expect(err).to.be.null;
            expect(res).to.exist;
            expect(JSON.parse.bind(JSON, res)).to.not.throw;
            d();
          });

          ChannelUser.cacheGet(redis, channelUserId, function(err, res){
            expect(err).to.be.null;
            expect(res).to.exist;
            expect(JSON.parse.bind(JSON, res)).to.not.throw;
            d();
          });
        });
      });
    });

    it('should not cache the channel, user and channelUser in redis after a unsuccessful command', function(done){
      User.createInDatabase(userName, createOpts, function(err, dbUser){
        expect(err).to.be.null;

        let channelUserId = {
          user: dbUser._id,
          channel: dbChannel._id
        };

        validTestCmd.id = 'errorCmd';
        svc.execCommand(validTestCmd, function(err){
          expect(err).to.have.property('message', 'operational error');

          let d = _.after(3, done);

          Channel.cacheGet(redis, dbChannel._id, function(err, res){
            expect(err).to.be.null;
            expect(res).to.not.exist;
            d();
          });

          User.cacheGet(redis, dbUser._id, function(err, res){
            expect(err).to.be.null;
            expect(res).to.not.exist;
            d();
          });

          ChannelUser.cacheGet(redis, channelUserId, function(err, res){
            expect(err).to.be.null;
            expect(res).to.not.exist;
            d();
          });
        });
      });
    });

    it('should publish the command\'s response output object in redis');
  });

  describe('#fillMissingCommandArgs', function(){
    let
      user,
      channelUser,
      sandbox,
      cmd;

    beforeEach(function(done){
      cmd = {
        id: 'testCmd',
        userName: userName,
        channelName: channelName,
        log: log,
        resolver: resolver,
        commandService: svc,
        channelService: 'twitch'
      };

      sandbox = sinon.sandbox.create();

      // doExecuteCommand is called from fillMissingCommandArgs. We want to avoid executing the command,
      // and instead forward directly to the test case's callback
      sandbox.stub(svc, 'doExecuteCommand', function(cmd, callback){
        callback(null);
      });

      User.createInDatabaseAndSetUp(userName, createOpts, createOpts, function(err, u){
        user = u;

        let args = _.extend({ user: u, channel: channel }, createOpts);
        ChannelUser.createInDatabaseAndSetUp({ user: user.id, channel: channel.id }, args, args, function(err, cU){
          channelUser = cU;
          done();
        });
      });
    });

    afterEach(function(){
      sandbox.restore();
    });

    function callHelper(res, cb){
      svc.fillMissingCommandArgs(cmd, cb, null, JSON.stringify(res));
    }

    it('should load a channel from the cache for a valid channelName to ID map and a cached channel', function(done){
      let spy = sandbox.spy(Channel, 'parseAndRestoreFromCache');

      callHelper({ channel: channel.toCacheValue() }, function(err){
        expect(err).to.be.null;
        expect(cmd.channel).to.be.an.instanceOf(Channel);
        expect(cmd.channel.id).to.eql(channel.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should load a channel from the database for a valid channelName to ID map and a not cached channel', function(done){
      let spy = sandbox.spy(svc.channelPolicies, 'getChannel');
      callHelper({ channel: false }, function(err){
        expect(err).to.be.null;
        expect(cmd.channel).to.be.an.instanceOf(Channel);
        expect(cmd.channel.id).to.eql(channel.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should load a channel from the cache for an invalid channelName to ID map and a cached channel', function(done){
      let
        loadFromCacheSpy = sandbox.spy(Channel, 'loadFromCache'),
        setUpFromDatabaseSpy = sandbox.spy(Channel, 'setUpFromDatabase');

      channel.cacheChanges(cmd, function(err){
        expect(err).to.be.null;

        callHelper({}, function(err){
          expect(err).to.be.null;
          expect(cmd.channel).to.be.an.instanceOf(Channel);
          expect(cmd.channel.id).to.eql(channel.id);
          expect(loadFromCacheSpy.calledOnce).to.be.true;
          expect(setUpFromDatabaseSpy.callCount).to.equal(0);
          done();
        });
      });
    });

    it('should load a channel from the database for an invalid channelName to ID map and a not cached channel', function(done){
      let spy = sandbox.spy(Channel, 'setUpFromDatabase');
      callHelper({}, function(err){
        expect(err).to.be.null;
        expect(cmd.channel).to.be.an.instanceOf(Channel);
        expect(cmd.channel.id).to.eql(channel.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should update the remote channelName to ID map if an invalid mapping happened', function(done){
      callHelper({}, function(err){
        expect(err).to.be.null;
        redis.hget(resolver.getChannelMapCacheKey(), cmd.channelName, function(err, id){
          expect(err).to.be.null;
          expect(id).to.equal(channel.id.id);
          done();
        });
      });
    });

    it('should update the local channelName to ID map if an invalid mapping happened', function(done){
      callHelper({}, function(err){
        expect(err).to.be.null;
        expect(resolver.getChannelId(channelName)).to.eql(channel.id);
        done();
      });
    });

    it('should set the channel state if a valid channelName to ID happened', function(done){
      callHelper({ joinStatus: 'joined' }, function(err){
        expect(err).to.be.null;
        expect(cmd).to.have.deep.property('channel.state', 'online');
        done();
      });
    });

    it('should set the channel state if an invalid channelName to ID happened', function(done){
      callHelper({ }, function(err){
        expect(err).to.be.null;
        expect(cmd).to.have.deep.property('channel.state', 'offline');
        done();
      });
    });

    it('should load a user from the cache for a valid userName to ID map and a cached user', function(done){
      let spy = sandbox.spy(User, 'parseAndRestoreFromCache');
      callHelper({ user: user.toCacheValue() }, function(err){
        expect(err).to.be.null;
        expect(cmd.user).to.be.an.instanceOf(User);
        expect(cmd.user.id).to.eql(user.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should load a user from the database for a valid userName to ID map and a not cached user', function(done){
      let spy = sandbox.spy(User, 'findInDatabaseOrCreateAndSetUp');

      callHelper({ user: false }, function(err){
        expect(err).to.be.null;
        expect(cmd.user).to.be.an.instanceOf(User);
        expect(cmd.user.id).to.eql(user.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should load a user from the cache for an invalid userName to ID map and a cached user', function(done){
      let
        loadFromCacheSpy = sandbox.spy(User, 'loadFromCache'),
        setUpFromDatabaseSpy = sandbox.spy(User, 'setUpFromDatabase');

      user.cacheChanges(cmd, function(err){
        expect(err).to.be.null;

        callHelper({}, function(err){
          expect(err).to.be.null;
          expect(cmd.user).to.be.an.instanceOf(User);
          expect(cmd.user.id).to.eql(user.id);
          expect(loadFromCacheSpy.calledOnce).to.be.true;
          expect(setUpFromDatabaseSpy.callCount).to.equal(0);
          done();
        });
      });
    });

    it('should load a user from the database for an invalid userName to ID map and a not cached user', function(done){
      let spy = sandbox.spy(User, 'setUpFromDatabase');
      callHelper({ }, function(err){
        expect(err).to.be.null;
        expect(cmd.user).to.be.an.instanceOf(User);
        expect(cmd.user.id).to.eql(user.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should update the remote userName to ID map if an invalid mapping happened', function(done){
      callHelper({}, function(err){
        expect(err).to.be.null;
        redis.hget(resolver.getUserMapCacheKey(), userName, function(err, id){
          expect(err).to.be.null;
          expect(id).to.equal(user.id.id);
          done();
        });
      });
    });

    it('should load a channelUser from the cache for a valid userName to ID map and a cached channelUser', function(done){
      let spy = sandbox.spy(ChannelUser, 'parseAndRestoreFromCache');
      callHelper({ channelUser: channelUser.toCacheValue() }, function(err){
        expect(err).to.be.null;
        expect(cmd.channelUser).to.be.an.instanceOf(ChannelUser);
        expect(cmd.channelUser.id).to.eql(channelUser.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should load a channelUser from the database for a valid userName to ID map and a not cached channelUser', function(done){
      let spy = sandbox.spy(ChannelUser, 'findInDatabaseOrCreateAndSetUp');
      callHelper({ channelUser: false }, function(err){
        expect(err).to.be.null;
        expect(cmd.channelUser).to.be.an.instanceOf(ChannelUser);
        expect(cmd.channelUser.id).to.eql(channelUser.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should load a channelUser from the cache for an invalid userName to ID map and a cached channelUser', function(done){
      let
        loadFromCacheSpy = sandbox.spy(ChannelUser, 'loadFromCache'),
        setUpFromDatabaseSpy = sandbox.spy(ChannelUser, 'setUpFromDatabase');

      channelUser.cacheChanges(cmd, function(err){
        expect(err).to.be.null;

        callHelper({}, function(err){
          expect(err).to.be.null;
          expect(cmd.channelUser).to.be.an.instanceOf(ChannelUser);
          expect(cmd.channelUser.id).to.eql(channelUser.id);
          expect(loadFromCacheSpy.calledOnce).to.be.true;
          expect(setUpFromDatabaseSpy.callCount).to.equal(0);
          done();
        });
      });
    });

    it('should load a channelUser from the database for an invalid userName to ID map and a not cached channelUser', function(done){
      let spy = sandbox.spy(ChannelUser, 'setUpFromDatabase');
      callHelper({ }, function(err){
        expect(err).to.be.null;
        expect(cmd.channelUser).to.be.an.instanceOf(ChannelUser);
        expect(cmd.channelUser.id).to.eql(channelUser.id);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });

    it('should create a new user if it doesn\'t exist', function(done){
      cmd.userName += _.uniqueId('newuser');
      callHelper({}, function(err){
        expect(err).to.be.null;
        expect(cmd.user).to.be.an.instanceOf(User);
        expect(cmd.user).to.have.deep.property('twitch.name', cmd.userName);
        done();
      });
    });

    it('should create a new channelUser if it doesn\'t exist', function(done){
      cmd.userName += _.uniqueId('newuser');
      callHelper({}, function(err){
        expect(err).to.be.null;
        expect(cmd.channelUser).to.be.an.instanceOf(ChannelUser);
        expect(cmd.channelUser).to.have.deep.property('user.twitch.name', cmd.userName);
        done();
      });
    });
  });

  describe('#doExecuteCommand', function(){
    let
      user,
      channelUser,
      sandbox,
      cmd;

    beforeEach(function(done){
      cmd = {
        id: 'testCmd',
        userName: userName,
        channelName: channelName,
        log: log,
        resolver: resolver,
        commandService: svc,
        channelService: 'twitch'
      };

      sandbox = sinon.sandbox.create();

      User.createInDatabaseAndSetUp(userName, createOpts, createOpts, function(err, u){
        user = u;

        let args = _.extend({ user: u, channel: channel }, createOpts);
        ChannelUser.createInDatabaseAndSetUp({ user: user.id, channel: channel.id }, args, args, function(err, cU){
          channelUser = cU;

          cmd.userId = user.id;
          cmd.channelId = channel.id;
          cmd.channel = channel;
          cmd.user = user;
          cmd.channelUser = channelUser;
          done();
        });
      });
    });

    afterEach(function(){
      sandbox.restore();
    });

    it('should execute a valid command', function(done){
      svc.doExecuteCommand(cmd, function(err, res){
        expect(err).to.be.null;
        expect(res).to.have.property('testExecuted', true);
        done();
      });
    });

    it('should not execute an out-of-state command', function(done){
      channel.state = 'paused';
      svc.doExecuteCommand(cmd, function(err){
        expect(err).to.have.property('message', 'BOT_CMD_NOT_AVAILABLE');
        done();
      });
    });

    it('should load and execute existing command that is not loaded into the command\'s channel', function(done){
      let spy = sandbox.spy(svc.pluginManager, 'setUpCommand');

      channel.loadedPlugins.splice(channel.loadedPlugins.indexOf('testPlugin'), 1);
      delete channel.tempStore.testPlugin;
      delete channel.pluginState.testPlugin;

      svc.doExecuteCommand(cmd, function(err, res){
        expect(err).to.be.null;
        expect(res).to.have.property('testExecuted', true);
        expect(spy.calledOnce).to.be.true;
        done();
      });
    });
  });
});