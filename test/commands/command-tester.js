"use strict";

const
  _ = require('lodash'),
  async = require('async'),  
  uuidGen = require('node-uuid'),

  CommandService = require('../../lib/command-service'),
  PluginManager = require('../../lib/plugin-manager.js'),
  db = require('thebot-db')(),
  User = db.User,
  Channel = db.Channel,
  ChannelUser = db.ChannelUser,
  TwitchResolver = db.TwitchResolver;
  
module.exports = CommandTester;
  
function CommandTester(){}

let
  pluginManager,
  cmd,
  cmdChannel,
  cmdOpts,
  extraChannelUsers = [];

let
  args = { resolver: new TwitchResolver() },
  userArgs = args,
  channelArgs = args;

CommandTester.installPlugin = function(plugin, cb){
  CommandTester.installPlugins([plugin], function(plugins){
    cb(plugins[0]);
  });
};

CommandTester.installPlugins = function(pluginNames, cb){
  let
    plugins = [],
    doneCb = _.after(pluginNames.length, cb.bind(null, plugins));
  
  pluginManager = new PluginManager();
  
  pluginNames.forEach(function(pluginName){
    pluginManager.loadPluginFile(__dirname + '/../../lib/commands/' + pluginName + '.js', function(err, plugin){
      expect(err).to.be.null;
      plugins.push(plugin);
      doneCb();
    });
  });
};

CommandTester.prepare = function(_cmd, opts, cb){
  cmd = _cmd;

  let name = uuidGen.v1();

  cmdOpts = _.defaults(opts, {
    id: cmd.id,
    channelName: '#' + name,
    userName: name,
    commandService: new CommandService(pluginManager),
    resolver: args.resolver
  });

  // Load the channel and the user beforehand, so it can be modified by the test case
  async.auto({
    channel: function(cb){
      async.waterfall([
        Channel.createInDatabaseAndSetUp.bind(Channel, cmdOpts.channelName, channelArgs, channelArgs),
        function(channel, cb){
          pluginManager.setUpCommands(channel.dbEntity, channel, function(err){
            cb(err, channel);
          });
        }
      ], cb);
    },
    user: function(cb){
      User.createInDatabaseAndSetUp(cmdOpts.userName, userArgs, userArgs, cb);
    },
    channelUser: ['channel', 'user', function(cb, res){
      let cuArgs = _.extend({}, args, res);
      ChannelUser.createInDatabaseAndSetUp({
        channel: res.channel.id,
        user: res.user.id
      }, cuArgs, cuArgs, cb);
    }]
  }, function(err, res){
    expect(err).to.not.exist;
    _.extend(cmdOpts, _.pick(res, ['channel', 'user', 'channelUser']));
    cmdChannel = res.channel;
    cb(cmdChannel, cmdOpts, cmdOpts.channelUser);
  });
  
  // Prevent output.message parsing, test for it to avoid infinite recursion on cmd reuse
  cmd.oldPackResponse = cmd.oldPackResponse || cmd.packResponse;
  cmd.packResponse = function(ch, res, callback){
    res._output = res.output;
    cmd.oldPackResponse(ch, res, function(err){
      expect(err).to.be.null;
      res.output = res._output;
      callback && callback(null);
    });
  };
};

CommandTester.run = function(cb){
  cmd.exec(cmdChannel, cmdOpts, cb);
};

CommandTester.prepareAndRun = function(_cmd, opts, cb){
  CommandTester.prepare(_cmd, opts, CommandTester.run.bind(CommandTester, cb));
};

CommandTester.genChannelUsers = function(amount, callback){
  async.times(amount, function(n, next){
    User.createInDatabaseAndSetUp(uuidGen.v1(), userArgs, userArgs, function(err, user){
      expect(err).to.be.null;

      let cuArgs = _.extend({}, args, { channel: cmdChannel, user: user });

      ChannelUser.createInDatabaseAndSetUp({
        channel: cmdChannel.id,
        user: user.id
      }, cuArgs, cuArgs, next);
    });
  }, function(err, channelUsers){
    expect(err).to.not.exist;
    extraChannelUsers.push.apply(extraChannelUsers, channelUsers);
    callback(channelUsers);
  });
};

CommandTester.doCleanup = function(done){
  if(cmd){
    global.doDbAndCacheClear(done);
    cmd = cmdChannel = cmdOpts = null;
    extraChannelUsers = [];
  } else {
    done(null);
  }
};