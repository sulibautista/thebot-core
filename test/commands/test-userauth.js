"use strict";

const
  _ = require('lodash'),
  cmdTester = require('./command-tester.js'),
  conf = require('thebot-conf'),
  redis = conf.redis(),
  db = require('thebot-db')(),
  User = db.User,
  ChannelUser = db.ChannelUser,
  Resolver = db.TwitchResolver;

describe('Plugin#userauth', function(){
  let userauth;

  beforeEach(function(done){
    cmdTester.installPlugin('userauth', function(_userauth){
      userauth = _userauth;
      done();
    });
  });

  afterEach(cmdTester.doCleanup);

  it('should be named "userauth"', function(){
    expect(userauth.pluginName).to.equal('userauth');
  });

  describe('!updatemods', function(){
    /* jshint bitwise: false */
    const
      modGroup = ChannelUser.NORMAL | ChannelUser.OWNER | ChannelUser.MOD,
      unmodGroup = ChannelUser.NORMAL | ChannelUser.OWNER;

    function command(){
      return userauth.commands.updatemods.online;
    }

    it('should export "online", "offline" and "paused" commands', function(){
      expect(userauth.commands).to.have.property('updatemods')
        .with.keys('online', 'offline', 'paused');
    });

    it('should be authorized to OWNERS users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.OWNERS);
    });

    it('should export "updatemods" trigger', function(){
      expect(command()).to.have.property('triggers')
        .that.eqls([{
          id: 'updatemods',
          args: [
            { name: 'mods', types: ['list{string}'] }
          ]
        }]);
    });

    it('should revoke MOD group to existing mods in the database', function(done){
      cmdTester.prepare(command(), { mods:[] }, function(ch, opts, channelUser){
        ChannelUser.model
          .update(channelUser.id, { $set: { groups: modGroup, 'mod.created': new Date() } })
          .exec(function(err){
            expect(err).to.be.null;

            cmdTester.run(function(err){
              expect(err).to.be.null;

              ChannelUser.findInDatabase(channelUser.id, null, function(err, dbChUser){
                expect(err).to.be.null;
                expect(dbChUser).to.have.property('groups', unmodGroup);
                done();
              });
            });
          });
      });
    });

    it('should invalidate existing mods\'s channelUser entries in the cache', function(done){
      cmdTester.prepare(command(), { mods:[] }, function(ch, opts, channelUser){

        channelUser.commitToCache(redis, function(err, res){
          expect(err).to.be.null;

          ChannelUser.model
            .update(channelUser.id, { $set: { groups: modGroup, 'mod.created': new Date() } })
            .exec(function(err){
              expect(err).to.be.null;

              cmdTester.run(function (err) {
                expect(err).to.be.null;

                ChannelUser.cacheGet(redis, channelUser.id, function (err, res) {
                  expect(err).to.be.null;
                  expect(res).to.not.exist;
                  done();
                });
              });
            });
        });
      });
    });

    it('should ignore new mods that aren\'t registered as bot Users', function(done){
      cmdTester.prepareAndRun(command(), { mods: ['test' + _.uniqueId() ] }, function(err){
        expect(err).to.be.null;
        done();
      });
    });

    it('should ignore new mods that aren\'t registered as bot ChannelUsers', function(done){
      const userName = 'test' + _.uniqueId();

      User.createInDatabase(userName, { resolver: new Resolver() }, function(err){
        expect(err).to.be.null;
        cmdTester.prepareAndRun(command(), { mods:[userName] }, function(err){
          expect(err).to.be.null;
          done();
        });
      });
    });

    it('should add MOD group to new mods in the database', function(done){
      cmdTester.prepare(command(), {}, function(ch, opts, channelUser){
        opts.mods = [opts.user.twitch.name];

        cmdTester.run(function(err){
          expect(err).to.be.null;

          ChannelUser.findInDatabase(channelUser.id, null, function(err, dbChUser){
            expect(err).to.be.null;
            expect(dbChUser).to.have.property('groups', modGroup);
            done();
          });
        });
      });
    });

    it('should invalidate new mod\'s channelUser entries in the cache', function(done){
      cmdTester.prepare(command(), {}, function(ch, opts, channelUser){
        opts.mods = [opts.user.twitch.name];

        channelUser.commitToCache(redis, function(err){
          expect(err).to.be.null;

          cmdTester.run(function (err) {
            expect(err).to.be.null;

            ChannelUser.cacheGet(redis, channelUser.id, function (err, res) {
              expect(err).to.be.null;
              expect(res).to.not.exist;
              done();
            });
          });
        });
      });
    });

  });

});