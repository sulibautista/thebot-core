"use strict";

const
  async = require('async'),
  
  cmdTester = require('./command-tester.js'),
  conf = require('thebot-conf'),
  redis = conf.redis(),
  joinQueue = require('thebot-joinqueue')(redis);
    
describe('Plugin#channelops', function(){
  let channelops;
  
  beforeEach(function(done){
    cmdTester.installPlugin('channelops', function(_join){
      channelops = _join;
      done();
    });
  });
  
  afterEach(cmdTester.doCleanup);

  describe('!join', function(){
    
    function command(status){
      return channelops.commands.join[status];
    }
    
    describe('[online]', function(){
    
      it('should enqueue a channel to the joinQueue', function(done){
        let
          hid,
          chId;
        
        async.waterfall([
          joinQueue.registerHandler.bind(joinQueue),
          function(_hid, cb){
            hid = _hid;
            cmdTester.prepare(command('offline'), {}, function(ch){
              cb(null, ch);
            });
          },
          function(ch, cb){
            chId = ch.id;
            cmdTester.run(cb);
          },
          function(res, cb){
            joinQueue.dequeueJoin(hid, cb);
          },
          function(queueChId, cb){
            expect(chId.id).to.equal(queueChId);
            
            joinQueue.getJoinStatus(chId.id, cb);
          },
          function(status, cb){
            expect(status).to.equal('joining');
            
            joinQueue.delJoinStatus(chId, function(err){
              cb(err);
            });
          }
        ], done);
      });
      
    });
    
  });
});