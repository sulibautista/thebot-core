"use strict";

const
  async = require('async'),
  cmdTester = require('./command-tester.js'),
  ChannelUser = require('thebot-db')().ChannelUser;

describe('Plugin#coins', function(){
  let coins;
    
  beforeEach(function(done){
    cmdTester.installPlugin('coins', function(_coins){
      coins = _coins;
      done();
    });
  });
  
  afterEach(cmdTester.doCleanup);
  
  it('should be named "coins"', function(){
    expect(coins.pluginName).to.equal('coins');
  });

  describe('!betcoins', function(){

    function command(){
      return coins.commands.betcoins.online;
    }

    it('should export an online command', function(){
      expect(coins.commands).to.have.deep.property('betcoins.online');
    });

    it('should be authorized to NORMAL users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.NORMAL);
    });

    it('should export "betcoins" trigger', function(){
      expect(command()).to.have.property('triggers')
        .that.eqls([{ id: 'betcoins', validPrefix: ['#'], aliases: ['bc'] }]);
    });

    it('should export "count" and "countnotitle" messages', function(){
      expect(command()).to.have.property('messages')
        .with.keys('count', 'countnotitle');
    });

    it('should output a "countnotitle" message', function(done){
      cmdTester.prepare(command(), {}, function(ch, opts, user){
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.deep.property('output.message', 'countnotitle');
          expect(res.output).to.have.property('user', user.name);
          expect(res.output).to.have.property('coins');
          expect(res.output).to.have.property('rank');
          done();
        });
      });
    });

    it('should output an user\'s coin amount', function(done){
      const coins = 999;

      cmdTester.prepare(command(), {}, function(ch, opts, channelUser){

        channelUser.setCurrencyCount('coins', coins, function(err){
          expect(err).to.be.null;

          cmdTester.run(function(err, res){
            expect(err).to.be.null;
            expect(res.output).to.have.property('coins', coins);
            done();
          });
        });
      });
    });

    it('should output an user\'s rank position', function(done){
      const usersWithMoreCoins = 5;

      cmdTester.prepare(command(), {}, function(){
        async.series([
          function(cb){
            cmdTester.genChannelUsers(usersWithMoreCoins, function(chUsers){
              async.each(chUsers, function(chUser, ok){
                chUser.dbEntity.coins.count = 99999;
                chUser.dbEntity.save(ok);
              }, cb);
            });
          },
          function(cb){
            cmdTester.run(function(err, res){
              expect(err).to.be.null;
              expect(res.output).to.have.property('rank', usersWithMoreCoins + 1);
              cb();
            });
          }
        ], done);
      });
    });
  });
  
  
  describe('!topcoins', function(){
    function command(){
      return coins.commands.topcoins.online;
    }
    
    it('should export an online command', function(){
      expect(coins.commands).to.have.deep.property('topcoins.online');
    });

    it('should be authorized to STAFF users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.STAFF);
    });

    it('should export "topcoins" and "top10coins" triggers', function(){
      expect(command()).to.have.deep.property('triggers[0]')
        .that.deep.eqls({
          id: 'topcoins',
          aliases: ['top10coins'],
          args: [
            { name: 'count', types: ['unsigned'], optional: true }
          ]
        });
    });

    it('should export "top" message', function(){
      expect(command()).to.have.property('messages')
        .with.keys('top');
    });

    it('should output a "top" message with a count of 10 users', function(done){
      cmdTester.prepareAndRun(command(), {}, function(err, res){
        expect(err).to.be.null;
        expect(res).to.have.deep.property('output.message', 'top');
        expect(res.output).to.have.property('count', 10);
        done();
      });
    });
    
    it('should output a list of 10 users ordered by coin amount DESC', function(done){
      doCommandWithUserCount(null, function(data){
        expect(data.res.output).to.have.property('list', data.list.join(', '));
        done();
      });
    });

    it('should output the specified number of users ordered by coin amount DESC', function(done){
      doCommandWithUserCount(5, function(data){
        expect(data.res.output).to.have.property('list', data.list.join(', '));
        done();
      });
    });

    it('should not output more than 20 user entries', function(done){
      doCommandWithUserCount(21, function(data){
        data.list.length = 20;
        expect(data.res.output).to.have.property('list', data.list.join(', '));
        done();
      });
    });

    function doCommandWithUserCount(userCount, callback){
      const
          realUserCount = userCount !== null? userCount : 10,
          baseCoins = 9999;

      let topList = [];

      cmdTester.prepare(command(), {}, function(ch, cmdOpts){
        userCount !== null && (cmdOpts.count = userCount);

        async.series([
          function(cb){
            cmdTester.genChannelUsers(realUserCount, function(chUsers){
              async.each(chUsers, function(chUser, ok){
                chUser.dbEntity.coins.count = baseCoins - topList.length;
                chUser.dbEntity.save(ok);
                topList.push(chUser.user.twitch.name + ' - ' + chUser.coins.count);
              }, function(err){
                cb(err, err? null : topList);
              });
            });
          },
          cmdTester.run.bind(cmdTester)
        ], function(err, res){
          expect(err).to.not.exist;
          callback({ list: res[0], res: res[1] });
        });
      });
    }
  });

  describe('!givecoins', function(){

    function command(){
      return coins.commands.givecoins.online;
    }

    it('should export an online command', function(){
      expect(coins.commands).to.have.deep.property('givecoins.online');
    });

    it('should be authorized to TRUSTED_STAFF users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.TRUSTED_STAFF);
    });

    it('should export "givecoins" trigger', function(){
      expect(command()).to.have.property('triggers')
        .that.eqls([
          {
            id: 'givecoins',
            aliases: ['addcoins', 'addcoin', 'givecoin'],
            args: [
              { name: 'toUser', types: ['string'] },
              { name: 'amount', types: ['integer'] }
            ]
          },
          {
            id: 'takecoins',
            aliases: ['removecoins', 'delcoins', 'takecoin', 'removecoin', 'delcoin'],
            aliasCommand: 'givecoins $toUser -$amount',
            args: [
              { name: 'toUser', types: ['string'] },
              { name: 'amount', types: ['unsigned'] }
            ]
          }
        ]);
    });

    it('should export "given" and "wronguser" messages', function(){
      expect(command()).to.have.property('messages')
        .with.keys('given', 'taken', 'wronguser');
    });

    it('should add coins to the specified user', function(done){
      const
        init = 100,
        added = 31;

      cmdTester.prepare(command(), {}, function(ch, cmdOpts){
        cmdTester.genChannelUsers(1, function(users){
          let toUser = users[0];

          cmdOpts.toUser = toUser.user.twitch.name;
          cmdOpts.amount = added;

          toUser.setCurrencyCount('coins', init, function(err){
            expect(err).to.be.null;

            cmdTester.run(function(err, res){
              expect(err).to.be.null;
              expect(res).to.have.deep.property('output.message', 'given');
              expect(res.output).to.have.property('coins', added);

              toUser.getCoins(function(err, coins){
                expect(err).to.be.null;
                expect(coins).to.equal(init + added);
                done();
              });
            });
          });
        });
      });
    });

    it('should remove coins to the specified user', function(done){
      const
        init = 100,
        added = -31;

      cmdTester.prepare(command(), {}, function(ch, cmdOpts){
        cmdTester.genChannelUsers(1, function(users){
          let toUser = users[0];

          cmdOpts.toUser = toUser.user.twitch.name;
          cmdOpts.amount = added;

          toUser.setCurrencyCount('coins', init, function(err){
            expect(err).to.be.null;

            cmdTester.run(function(err, res){
              expect(err).to.be.null;
              expect(res).to.have.deep.property('output.message', 'taken');
              expect(res.output).to.have.property('coins', Math.abs(added));

              toUser.getCoins(function(err, coins){
                expect(err).to.be.null;
                expect(coins).to.equal(init + added);
                done();
              });
            });
          });
        });
      });
    });

    it('should report a non-existent user', function(done){
      const wrongUser = 'nonexistent';

      cmdTester.prepareAndRun(command(), { toUser: wrongUser, amount: 10 }, function(err, res){
        expect(err).to.be.null;
        expect(res).to.have.deep.property('output.message', 'wronguser');
        expect(res.output).to.have.property('user', wrongUser);
        done();
      });
    });
  });

  describe('!resetallcoins', function(){
    function command(){
      return coins.commands.resetallcoins.online;
    }

    it('should export an online command', function(){
      expect(coins.commands).to.have.deep.property('resetallcoins.online');
    });

    it('should be authorized to TRUSTED_STAFF users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.TRUSTED_STAFF);
    });

    it('should export "resetallcoins" and "resetallbc" triggers', function(){
      expect(command()).to.have.deep.property('triggers[0]')
        .that.eqls({
          id: 'resetallcoins',
          aliases: ['resetallbc']
        });
    });

    it('should export "reset" message', function(){
      expect(command()).to.have.property('messages')
          .with.keys('reset');
    });

    it('should reset all user\'s coins to channel\'s initial value', function(done){
      const initialValue = 50;

      cmdTester.prepare(command(), {}, function(ch, cmdOpts){
        cmdOpts.channelUser.setCurrencyCount('coins', 99999, function(err){
          expect(err).to.be.null;
          ch.coins.initial = initialValue;

          cmdTester.run(function(err){
            expect(err).to.be.null;

            cmdOpts.channelUser.getCoins(function(err, coins){
              expect(err).to.be.null;
              expect(coins).to.equal(initialValue);
              done();
            });
          });
        });
      });
    });

    it('should not reset other channels\' user coins', function(done){
      const sameCoins = 99999;

      cmdTester.prepare(command(), {}, function(ch, cmdOpts){
        cmdOpts.channelUser.setCurrencyCount('coins', sameCoins, function(err){
          expect(err).to.be.null;

          cmdTester.prepareAndRun(command(), {}, function(err){
            expect(err).to.be.null;

            cmdOpts.channelUser.getCoins(function(err, coins){
              expect(err).to.be.null;
              expect(coins).to.equal(sameCoins);
              done();
            });
          });
        });
      });
    });
  });

});