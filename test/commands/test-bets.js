"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  spies = require('chai-spies'),
  cmdTester = require('./command-tester.js'),
  ChannelUser = require('thebot-db')().ChannelUser;

chai.use(spies);

describe('Plugin#bets', function(){
  let bets;

  beforeEach(function(done){
    cmdTester.installPlugin('bets', function(_bets){
      bets = _bets;
      done();
    });
  });

  afterEach(cmdTester.doCleanup);

  it('should be named "bets"', function(){
    expect(bets.pluginName).to.equal('bets');
  });

  it('should start in "stopped" state', function(){
    expect(bets.startingState).to.equal('stopped');
  });

  describe('!startbets', function(){
    function command(){
      return bets.commands.startbets['online.stopped'];
    }

    it('should export an "online.stopped" command', function(){
      expect(bets.commands).to.have.property('startbets')
        .with.property('online.stopped');
    });

    it('shoud be authorized to TRUSTED_STAFF users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.TRUSTED_STAFF);
    });

    it('should export a "startbets", "scorebets" and "teambets" triggers', function(){
      expect(command()).to.have.property('triggers')
        .that.deep.eqls([
          {
            id: 'startbets',
            args: [
              { name: 'type', types: ['set{score,team}'], optional: true },
              { name: 'teamOne', types: ['string'], optional: true },
              { name: 'teamTwo', types: ['string'], optional: true }
            ]
          },
          {
            id: 'scorebets',
            aliases: ['solobets'],
            aliasCommand: 'startbets score $teamOne',
            args: [
              { name: 'teamOne', types: ['string'], optional:  true }
            ]
          },
          {
            id: 'teambets',
            aliasCommand: 'startbets team $teamOne $teamTwo',
            args: [
              { name: 'teamOne', types: ['string'], optional:  true },
              { name: 'teamTwo', types: ['string'], optional:  true }
            ]
          }
        ]);
    });

    it('should export "scoreplacebet", "scorebetstarts", "teamplacebet", "teambetstarts" messages', function(){
      expect(command()).to.have.property('messages')
        .with.keys('scoreplacebets', 'scorebetstarts', 'teamplacebets', 'teambetstarts');
    });

    it('should fail to run if called without type and teams, and no previous bet has ran', function(done){
      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);
      cmd.setPluginState = chai.spy(cmd.setPluginState);

      cmdTester.prepareAndRun(cmd, {}, function(err, res){
        expect(err).to.exist;
        expect(res).to.not.exist;
        expect(cmd.saveStored).to.not.have.been.called;
        expect(cmd.setPluginState).to.not.have.been.called;
        done();
      });
    });

    it('should fail to run if called with conflicting team names', function(done){
      this.timeout(3000);

      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);
      cmd.setPluginState = chai.spy(cmd.setPluginState);

      function checkError(cb, err, res){
        expect(err).to.exist;
        expect(res).to.not.exist;
        expect(cmd.saveStored).to.not.have.been.called;
        expect(cmd.setPluginState).to.not.have.been.called;
        cmdTester.doCleanup(cb);
      }

      async.series([
        function(cb){
          cmdTester.prepareAndRun(cmd, { type: 'team', teamOne: 'tie', teamTwo: 't2' }, checkError.bind(null, cb));
        },
        function(cb){
          cmdTester.prepareAndRun(cmd, { type: 'team', teamOne: 't1', teamTwo: 'teambets' }, checkError.bind(null, cb));
        },
        function(cb){
          cmdTester.prepareAndRun(cmd, { type: 'team', teamOne: 'bet', teamTwo: 't2' }, checkError.bind(null, cb));
        },
        function(cb){
          cmdTester.prepareAndRun(cmd, { type: 'score', teamOne: 'tie' }, checkError.bind(null, cb));
        },
        function(cb){
          cmdTester.prepareAndRun(cmd, { type: 'score', teamOne: 'startbets' }, checkError.bind(null, cb));
        }
      ], function(err){
        expect(err).to.not.exist;
        done();
      });
    });

    it('should fail to run if called with type=team and just one team', function(done){
      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);
      cmd.setPluginState = chai.spy(cmd.setPluginState);

      cmdTester.prepareAndRun(cmd, { type: 'team', teamOne: 't1' }, function(err, res){
        expect(err).to.exist;
        expect(res).to.not.exist;
        expect(cmd.saveStored).to.not.have.been.called;
        expect(cmd.setPluginState).to.not.have.been.called;
        done();
      });
    });

    it('should fail to run if called with type=Team and duplicated teams', function(done){
      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);
      cmd.setPluginState = chai.spy(cmd.setPluginState);

      cmdTester.prepareAndRun(cmd, { type: 'team', teamOne: 't1', teamTwo: 't1' }, function(err, res){
        expect(err).to.exist;
        expect(res).to.not.exist;
        expect(cmd.saveStored).to.not.have.been.called;
        expect(cmd.setPluginState).to.not.have.been.called;
        done();
      });
    });

    it('should run a team betting contest when called with type=team', function(done){
      const
        team1 = 't1',
        team2 = 't2';

      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);

      cmdTester.prepare(cmd, { type: 'team', teamOne: team1, teamTwo: team2 }, function(ch){
        cmdTester.run(doTeamSuccessChecks.bind(null, ch, team1, team2, done));
      });
    });

    it('should fail to run a score betting when called with no teams', function(done){
      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);
      cmd.setPluginState = chai.spy(cmd.setPluginState);

      cmdTester.prepareAndRun(cmd, { type: 'score' }, function(err, res){
        expect(err).to.exist;
        expect(res).to.not.exist;
        expect(cmd.saveStored).to.not.have.been.called;
        done();
      });
    });

    it('should run a score betting contest when called with type=score', function(done) {
      const team1 = 't1';

      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);

      cmdTester.prepare(cmd, { type: 'score', teamOne: team1 }, function (ch) {
        cmdTester.run(doScoreSuccessChecks.bind(null, ch, team1, done));
      });
    });

    it('should run a score betting contest ignoring all teams but the first', function(done){
      const team1 = 't1';

      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);

      cmdTester.prepare(cmd, { type: 'score', teamOne: team1, teamTwo: 't2' }, function (ch) {
        cmdTester.run(doScoreSuccessChecks.bind(null, ch, team1, done));
      });
    });

    it('should run the last team betting contest when called without teams and type=team', function(done){
      const
        team1 = 't1',
        team2 = 't2';

      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);

      cmdTester.prepare(cmd, { type: 'team' }, function (ch) {
        let state = cmd.getStored(ch);
        state.previousRunType = 'score';
        state.previousRuns = {
          team: {
            teams: [team1, team2, 'tie']
          },
          score: {
            teams: [team1, 'tie']
          }
        };

        cmdTester.run(doTeamSuccessChecks.bind(null, ch, team1, team2, done));
      });
    });

    it('should run the last score betting contest when called without teams and type=score', function(done){
      const
        team1 = 't1',
        team2 = 't2';

      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);

      cmdTester.prepare(cmd, { type: 'score' }, function (ch) {
        let state = cmd.getStored(ch);
        state.previousRunType = 'team';
        state.previousRuns = {
          team: {
            teams: [team1, team2, 'tie']
          },
          score: {
            teams: [team1, 'tie']
          }
        };

        cmdTester.run(doScoreSuccessChecks.bind(null, ch, team1, done));
      });
    });

    it('should run the last betting contest when called without type and and teams', function(done){
      const
        team1 = 't1',
        team2 = 't2';

      let cmd = command();
      cmd.saveStored = chai.spy(cmd.saveStored);

      cmdTester.prepare(cmd, {}, function (ch) {
        let state = cmd.getStored(ch);
        state.previousRunType = 'score';
        state.previousRuns = {
          team: {
            teams: [team1, team2, 'tie']
          },
          score: {
            teams: [team1, 'tie']
          }
        };

        cmdTester.run(doScoreSuccessChecks.bind(null, ch, team1, done));
      });
    });

    function doTeamSuccessChecks(ch, team1, team2, done, err, res){
      expect(err).to.be.null;
      expect(res).to.have.property('output')
        .that.eqls({ message: 'teamplacebets', teamOne: team1, teamTwo: team2 });

      expect(res).to.have.property('addCommands')
        .that.eqls([
          {
            id: team1,
            aliasCommand: 'setbet ' + team1 + ' $amount $score',
            args: [
              { name:'amount', types:['unsigned', 'set{max}'] },
              { name:'score', types:['integer'], optional: true }
            ]
          },
          {
            id: team2,
            aliasCommand: 'setbet ' + team2 + ' $amount $score',
            args: [
              { name:'amount', types:['unsigned', 'set{max}'] },
              { name:'score', types:['integer'], optional: true }
            ]
          },
          {
            "id": "tie",
            "aliasCommand": "setbet tie $amount $score",
            args: [
              { name:'amount', types:['unsigned', 'set{max}'] },
              { name:'score', types:['integer'], optional: true }
            ]
          }
        ]);

      let
        cmd = command(),
        stored = cmd.getStored(ch);

      expect(stored).to.have.property('betType', 'team');
      expect(stored).to.have.property('teams').eql([team1, team2, 'tie']);
      expect(stored).to.have.property('bets').eql({});

      expect(cmd.saveStored).to.have.been.called.once;

      expect(cmd.getPluginState(ch)).to.equal('betting');

      done();
    }

    function doScoreSuccessChecks(ch, team1, done, err, res){
      expect(err).to.be.null;
      expect(res).to.have.property('output')
        .that.eqls({ message: 'scoreplacebets', teamOne: team1 });

      expect(res).to.have.property('addCommands')
        .that.eqls([
          {
            id: team1,
            aliasCommand: 'setbet ' + team1 + ' $amount $score',
            args: [
              { name: 'amount', types: ['unsigned', 'set{max}'] },
              { name: 'score', types: ['integer'], optional: true }
            ]
          },
          {
            "id": "tie",
            "aliasCommand": "setbet tie $amount $score",
            args: [
              { name: 'amount', types: ['unsigned', 'set{max}'] },
              { name: 'score', types: ['integer'], optional: true }
            ]
          }
        ]);

      let
        cmd = command(),
        stored = command().getStored(ch);

      expect(stored).to.have.property('betType', 'score');
      expect(stored).to.have.property('teams').eql([team1, 'tie']);
      expect(stored).to.have.property('bets').eql({});

      expect(cmd.saveStored).to.have.been.called.once;

      expect(cmd.getPluginState(ch)).to.equal('betting');

      done();
    }
  });

  describe('!cancelbets', function(){
    function command(state){
      return bets.commands.cancelbets['online.' + state];
    }

    function eachState(fn){
      ['betting', 'playing'].forEach(function(s){
        fn(command(s), s);
      });
    }

    it('should export "online.betting" and "online.playing" commands', function(){
      expect(bets.commands).to.have.property('cancelbets')
        .with.keys('online.betting', 'online.playing');
    });

    it('should be authorized to TRUSTED_STAFF users by default', function(){
      eachState(function(cmd){
        expect(cmd).to.have.property('auth', ChannelUser.TRUSTED_STAFF);
      });
    });

    it('should export a "cancelbets" trigger', function(){
      eachState(function(cmd){
        expect(cmd).to.have.property('triggers')
          .that.eqls([
            {
              id: 'cancelbets'
            }
          ]);
      });
    });

    it('should export "reset" message', function(){
      eachState(function(cmd){
        expect(cmd).to.have.property('messages')
          .with.keys('canceled');
      });
    });

    it('should cancel the current betting contest', function(done){
      let ops = [];

      eachState(function(cmd, state){
        ops.push(function(cb){
          cmdTester.prepare(cmd, {}, function(ch){
            cmd.setPluginState(ch, state);
            setStored(cmd, ch, 'team', 't1', 't2');

            cmdTester.run(function(err, res){
              expect(err).to.be.null;
              expect(res).to.have.deep.property('output.message', 'canceled');
              expect(res).to.have.property('delCommands');
              expect(cmd.getPluginState(ch)).to.equal('stopped');
              cmdTester.doCleanup(cb);
            });
          });
        });
      });

      async.series(ops, done);
    });

    it('should remove score !setbet subcommands if on betting state', function(done){
      let cmd = command('betting');
      cmdTester.prepare(cmd, {}, function(ch){
        cmd.setPluginState(ch, 'betting');
        setStored(cmd, ch, 'team', 't1');

        cmdTester.run(function(err, res){
          expect(err).to.not.exist;
          expect(res).to.have.property('delCommands')
            .that.eqls(['t1', 'tie']);
          done();
        });
      });
    });

    it('should remove team !setbet subcommands if on betting state', function(done){
      let cmd = command('betting');
      cmdTester.prepare(cmd, {}, function(ch){
        cmd.setPluginState(ch, 'betting');
        setStored(cmd, ch, 'team', 't1', 't2');

        cmdTester.run(function(err, res){
          expect(err).to.not.exist;
          expect(res).to.have.property('delCommands')
            .that.eqls(['t1', 't2', 'tie']);
          done();
        });
      });
    });

    it('should remove score !betwins subcommands if on playing state', function(done){
      let cmd = command('playing');
      cmdTester.prepare(cmd, {}, function(ch){
        cmd.setPluginState(ch, 'playing');
        setStored(cmd, ch, 'score', 't1');

        cmdTester.run(function(err, res){
          expect(err).to.not.exist;
          expect(res).to.have.property('delCommands')
            .that.eqls(['t1wins', 't1loses', 't1ties']);
          done();
        });
      });
    });

    it('should remove team !betwins subcommands if on playing state', function(done){
      let cmd = command('playing');
      cmdTester.prepare(cmd, {}, function(ch){
        cmd.setPluginState(ch, 'playing');
        setStored(cmd, ch, 'team', 't1', 't2');

        cmdTester.run(function(err, res){
          expect(err).to.not.exist;
          expect(res).to.have.property('delCommands')
            .that.eqls(['t1wins', 't2wins']);
          done();
        });
      });
    });

    function setStored(cmd, ch, betType, team1, team2){
      _.extend(cmd.getStored(ch), {
        betType: betType,
        teams: _.compact([team1, team2, 'tie']),
        bets: {}
      });
    }
  });

  describe('!setbet', function(){
    const
      team1 = 't1',
      team2 = 't2';

    function command(){
      return bets.commands.setbet['online.betting'];
    }

    it('should export an "setbet" command', function(){
      expect(bets.commands).to.have.property('setbet')
        .with.property('online.betting');
    });

    it('shoud be authorized to NORMAL users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.NORMAL);
    });

    it('should export a "setbet" trigger', function(){
      expect(command()).to.have.property('triggers')
        .that.deep.eqls([
          {
            id: 'setbet',
            validPrefix: ['#'],
            args: [
              { name: 'team', types: ['string'] },
              { name: 'amount', types: ['unsigned', 'set{max}'] },
              { name: 'score', types: ['integer'], optional: true }
            ]
          }
        ]);
    });

    it('should fail to set a bet for an unknown team', function(done){
      let cmd = command();
      cmdTester.prepare(cmd, { team: 'invalidteam', amount: 1 }, function(ch){
        setStored(cmd, ch, 'team');
        cmdTester.run(function(err){
          expect(err).to.have.property('message', 'wrong team name');
          done();
        });
      });
    });

    it('should fail to set a score bet without a score', function(done){
      let cmd = command();
      cmdTester.prepare(cmd, { team: team1, amount: 1 }, function(ch){
        setStored(cmd, ch, 'score');
        cmdTester.run(function(err){
          expect(err).to.have.property('message', 'missing score argument');
          done();
        });
      });
    });

    it('should fail to set a zero coins bet', function(done){
      let cmd = command();
      cmdTester.prepare(cmd, { team: team1, amount: 0 }, function(ch){
        setStored(cmd, ch, 'team');
        cmdTester.run(function(err){
          expect(err).to.have.property('message', 'invalid bet amount');
          done();
        });
      });
    });

    it('should set an user\'s team bet', function(done){
      const amount = 1;

      let cmd = command();
      cmdTester.prepare(cmd, { team: team2, amount: amount }, function(ch, cmdOpts){
        setStored(cmd, ch, 'team');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.be.an.Object;
          expect(cmd.getStored(ch).bets).to.have.property(cmdOpts.user.id)
            .that.eqls({ 'team': team2, amount: amount, score: 0 });
          done();
        });
      });
    });

    it('should set an user\'s score bet', function(done){
      const
        amount = 1,
        score = 1;

      let cmd = command();
      cmdTester.prepare(cmd, { team: team1, amount: amount, score: score }, function(ch, cmdOpts){
        setStored(cmd, ch, 'score');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.be.an.Object;
          expect(cmd.getStored(ch).bets).to.have.property(cmdOpts.user.id)
            .that.eqls({ 'team': team1, amount: amount, score: score });
          done();
        });
      });
    });

    it('should accept an user\'s score bet on "tie" without an score', function(done){
      const amount = 1;

      let cmd = command();
      cmdTester.prepare(cmd, { team: 'tie', amount: amount }, function(ch, cmdOpts){
        setStored(cmd, ch, 'score');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.be.an.Object;
          expect(cmd.getStored(ch).bets).to.have.property(cmdOpts.user.id)
            .that.eqls({ 'team': 'tie', amount: amount, score: 0 });
          done();
        });
      });
    });

    it('should rewrite an user\'s score bet to "tie" if the given bet score is zero', function(done){
      const amount = 1;

      let cmd = command();
      cmdTester.prepare(cmd, { team: team1, amount: amount, score: 0 }, function(ch, cmdOpts){
        setStored(cmd, ch, 'score');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.be.an.Object;
          expect(cmd.getStored(ch).bets).to.have.property(cmdOpts.user.id)
            .that.eqls({ 'team': 'tie', amount: amount, score: 0 });
          done();
        });
      });
    });

    it('should set an user\'s bet to her total amount of coins if the bet amount is greater than it', function(done){
      const
        userCoins = 500,
        amount = 999;

      expect(userCoins).to.be.lt(amount);

      let cmd = command();
      cmdTester.prepare(cmd, {  team: team1, amount: amount }, function(ch, cmdOpts){
        setStored(cmd, ch, 'team');
        cmdOpts.channelUser.setCurrencyCount('coins', userCoins, function(err){
          expect(err).to.be.null;
          cmdTester.run(function(err, res){
            expect(err).to.be.null;
            expect(res).to.be.an.Object;
            expect(cmd.getStored(ch).bets).to.have.property(cmdOpts.user.id)
              .that.eqls({ 'team': team1, amount: userCoins, score: 0 });
            done();
          });
        });
      });
    });

    it('should set an user\'s bet to her total amount of coins when amount=max', function(done){
      const userCoins = 999;

      let cmd = command();
      cmdTester.prepare(cmd, {  team: team1, amount: 'max' }, function(ch, cmdOpts){
        setStored(cmd, ch, 'team');
        cmdOpts.channelUser.setCurrencyCount('coins', userCoins, function(err){
          expect(err).to.be.null;
          cmdTester.run(function(err, res){
            expect(err).to.be.null;
            expect(res).to.be.an.Object;
            expect(cmd.getStored(ch).bets).to.have.property(cmdOpts.user.id)
              .that.eqls({ 'team': team1, amount: userCoins, score: 0 });
            done();
          });
        });
      });
    });


    function setStored(cmd, ch, betType){
      _.extend(cmd.getStored(ch), {
        betType: betType,
        teams: betType === 'team'? [team1, team2, 'tie'] : [team1, 'tie'],
        bets: {}
      });
    }
  });

  describe('!endbets', function(){
    const
      team1 = 't1',
      team2 = 't2';

    function command(){
      return bets.commands.endbets['online.betting'];
    }

    it('should export an "endbets" command', function(){
      expect(bets.commands).to.have.property('endbets')
        .with.property('online.betting');
    });

    it('shoud be authorized to TRUSTED_STAFF users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.TRUSTED_STAFF);
    });

    it('should export an "endbets" trigger', function(){
      expect(command()).to.have.property('triggers')
        .that.deep.eqls([
          {
            id: 'endbets',
            aliases: ['closebets'],
            args: [
              { name: 'time', types: ['unsigned', 'set{now}'], optional: true }
            ]
          }
        ]);
    });

    it('should export "endsin" and "closed" messages', function(){
      expect(command()).to.have.property('messages')
        .with.keys('endsin', 'closed');
    });

    it('should end a betting contest in 60s by default', function(done){
      const defTime = 60;

      cmdTester.prepare(command(), {}, function(ch, cmdOpts){
        initTeam(ch);

        let doEnd = doStubs(defTime, cmdOpts);

        cmdTester.run(function(err, res){
          checkEndsInRes(defTime, cmdOpts, err, res);

          doEnd(function(err, res){
            checkClosedRes(cmdOpts, err, res);
            done();
          });
        });
      });
    });

    it('should use the last ending time if used multiple times during the waiting window', function(done){
      const
        firstTime = 5,
        secondTime = 10;

      let
        cmd = command(),
        firstOpts,
        secondOpts,
        firstEnd,
        secondEnd;

      async.waterfall([
        function(cb){
          cmdTester.prepare(cmd, { time: firstTime }, function(ch, cmdOpts){
            initTeam(ch);
            firstOpts = cmdOpts;
            firstEnd = doStubs(firstTime, firstOpts);
            cmdTester.run(cb);
          });
        },
        function(res, cb){
          checkEndsInRes(firstTime, firstOpts, null, res);

          secondOpts = _.defaults({ time: secondTime }, firstOpts);
          secondEnd = doStubs(secondTime, secondOpts);
          cmd.exec(secondOpts.channel, secondOpts, cb);
        },
        function(res, cb){
          checkEndsInRes(secondTime, secondOpts, null, res);

          firstEnd(function(err){
            expect(err).to.have.property('message', 'operation got cancelled');

            secondEnd(function(err, res){
              checkClosedRes(secondOpts, err, res);
              cb(null);
            });
          });
        }
      ], done);
    });

    it('should end a betting contest immediately when called with time=now', function(done){
      let cmd = command();

      cmdTester.prepare(cmd, { time: 'now' }, function(ch, cmdOpts){
        initTeam(ch);

        command().saveStored = chai.spy(cmd.saveStored);

        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(cmd.getPluginState(cmdOpts.channel)).to.equal('playing');
          expect(cmd.saveStored).to.have.been.called.once;
          expect(res).to.have.deep.property('output.message', 'closed');
          checkTeamCmdChanges(res);
          done();
        });
      });
    });

    it('should end a betting contest immediately when called with time=0', function(done){
      let cmd = command();

      cmdTester.prepare(cmd, { time: 0 }, function(ch, cmdOpts){
        initScore(ch);

        command().saveStored = chai.spy(cmd.saveStored);

        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(cmd.getPluginState(cmdOpts.channel)).to.equal('playing');
          expect(cmd.saveStored).to.have.been.called.once;
          expect(res).to.have.deep.property('output.message', 'closed');
          checkScoreCmdChanges(res);
          done();
        });
      });
    });

    it('should cancel the schedule of a betting contest if ended now', function(done){
      const time = 60;

      let cmd = command();

      cmdTester.prepare(cmd, {}, function(ch, cmdOpts){
        initScore(ch);

        let doEnd = doStubs(time, cmdOpts);

        cmdTester.run(function(err, res){
          checkEndsInRes(time, cmdOpts, err, res);

          cmd.exec(ch, _.defaults({ time: 'now' }, cmdOpts), function(err, res){
            checkClosedRes(cmdOpts, err, res);

            doEnd(function(err, res){
              expect(err).to.have.property('message', 'operation got cancelled');
              done();
            });
          });
        });
      });
    });

    function initTeam(ch){
      setStored(command(), ch, 'team');
    }

    function initScore(ch){
      setStored(command(), ch, 'score');
    }

    function doStubs(expectedTime, cmdOpts){
      let
        endCmdOpts,
        time;

      command().saveStored = chai.spy(command().saveStored);

      cmdOpts.commandService.scheduleCommand = chai.spy(function(cmdOpts, _time){
        endCmdOpts = cmdOpts;
        time = _time;
      });

      return function runScheduledEnd(cb){
        expect(time).to.equal(expectedTime*1000);

        _.extend(endCmdOpts, _.pick(cmdOpts, ['user', 'channelUser', 'channel']));

        command().exec(cmdOpts.channel, endCmdOpts, cb);
      };
    }

    function checkEndsInRes(time, cmdOpts, err, res){
      expect(err).to.be.null;
      expect(res).to.have.property('output')
        .that.eqls({ message: 'endsin', time: time });
      expect(command().getStored(cmdOpts.channel)).to.have.property('pendingEnd')
        .that.is.closeTo(Date.now() + time*1000, 1000);
      expect(command().saveStored).to.have.been.called.once;
      expect(cmdOpts.commandService.scheduleCommand).to.have.been.called.once;
    }

    function checkClosedRes(cmdOpts, err, res){
      expect(err).to.be.null;
      expect(command().getStored(cmdOpts.channel)).to.not.have.property('pendingEnd');
      expect(command().getPluginState(cmdOpts.channel)).to.equal('playing');
      expect(command().saveStored).to.have.been.called.twice;
      expect(res).to.have.deep.property('output.message', 'closed');

      let betType = command().getStored(cmdOpts.channel).betType;
      if(betType === 'team'){
        checkTeamCmdChanges(res);
      } else if(betType === 'score'){
        checkScoreCmdChanges(res);
      } else {
        throw new Error('unknown betType ' + betType);
      }
    }

    function checkTeamCmdChanges(res){
      expect(res).to.have.property('delCommands')
        .that.eql([team1, team2, 'tie']);
      expect(res).to.have.property('addCommands')
        .that.eql([
          {
            "id": team1 + "wins",
            "aliasCommand": "betwins t1 $result $bonus",
            "args": [
              { "name": "result", "optional": true, "types": ["integer"] },
              { "name": "bonus", "optional": true, "types": ["unsigned"] }
            ]
          },
          {
            "id": team2 + "wins",
            "aliasCommand": "betwins t2 $result $bonus",
            "args": [
              { "name": "result", "optional": true, "types": ["integer"] },
              { "name": "bonus", "optional": true, "types": ["unsigned"] }
            ]
          }
        ]);
    }

    function checkScoreCmdChanges(res){
      expect(res).to.have.property('delCommands')
        .that.eql([team1, 'tie']);
      expect(res).to.have.property('addCommands')
        .that.eql([
          {
            "id": team1 + "wins",
            "aliasCommand": "betwins " + team1 + " $result $bonus",
            "args": [
              { "name": "result", "optional": true, "types": ["integer"] },
              { "name": "bonus", "optional": true, "types": ["unsigned"] }
            ]
          },
          {
            "id": team1 + "loses",
            "aliasCommand": "betwins " + team1 + " -$result $bonus",
            "args": [
              { "name": "result", "optional": true, "types": ["unsigned"] },
              { "name": "bonus", "optional": true, "types": ["unsigned"] }
            ]
          },
          {
            "id": team1 + "ties",
            "aliasCommand": "betwins tie $result $bonus",
            "args": [
              { "name": "result", "optional": true, "types": ["integer"] },
              { "name": "bonus", "optional": true, "types": ["unsigned"] }
            ]
          }
        ]);
    }

    function setStored(cmd, ch, betType){
      _.extend(cmd.getStored(ch), {
        betType: betType,
        teams: betType === 'team'? [team1, team2, 'tie'] : [team1, 'tie'],
        bets: {}
      });
    }
  });

  describe('!openbets', function(){
    const
      team1 = 't1',
      team2 = 't2';

    function command(){
      return bets.commands.openbets['online.playing'];
    }

    it('should export an "openbets" command', function(){
      expect(bets.commands).to.have.property('openbets')
        .with.property('online.playing');
    });

    it('shoud be authorized to TRUSTED_STAFF users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.TRUSTED_STAFF);
    });

    it('should export an "openbets" trigger', function(){
      expect(command()).to.have.property('triggers')
        .that.deep.eqls([
          {
            id: 'openbets'
          }
        ]);
    });

    it('should export an "opened" message', function(){
      expect(command()).to.have.property('messages')
        .with.keys('opened');
    });

    it('should re-open the current betting contest', function(done){
      runCommand('team', function(res, ch){
        expect(command().getPluginState(ch)).to.equal('betting');
        expect(res).to.have.deep.property('output.message', 'opened');
        done();
      });
    });

    it('should add score !setbets subcommands', function(done){
      runCommand('score', function(res){
        expect(res).to.have.property('addCommands')
          .that.eqls([
            {
              id: team1,
              aliasCommand: 'setbet ' + team1 + ' $amount $score',
              args: [
                { name: 'amount', types: ['unsigned', 'set{max}'] },
                { name: 'score', types: ['integer'], optional: true }

              ]
            },
            {
              "id": "tie",
              "aliasCommand": "setbet tie $amount $score",
              args: [
                { name: 'amount', types: ['unsigned', 'set{max}'] },
                { name: 'score', types: ['integer'], optional: true }
              ]
            }
          ]);

        done();
      });
    });

    it('should add team !setbet subcommands', function(done){
      runCommand('team', function(res){
        expect(res).to.have.property('addCommands')
          .that.eql([
            {
              id: team1,
              aliasCommand: 'setbet ' + team1 + ' $amount $score',
              args: [
                { name:'amount', types:['unsigned', 'set{max}'] },
                { name:'score', types:['integer'], optional: true }
              ]
            },
            {
              id: team2,
              aliasCommand: 'setbet ' + team2 + ' $amount $score',
              args: [
                { name:'amount', types:['unsigned', 'set{max}'] },
                { name:'score', types:['integer'], optional: true }
              ]
            },
            {
              "id": "tie",
              "aliasCommand": "setbet tie $amount $score",
              args: [
                { name:'amount', types:['unsigned', 'set{max}'] },
                { name:'score', types:['integer'], optional: true }
              ]
            }
          ]);
        done();
      });
    });

    it('should remove score !betwins subcommands', function(done){
      runCommand('score', function(res){
        expect(res).to.have.property('delCommands')
          .that.eql([team1 + 'wins', team1 + 'loses', team1 + 'ties']);
        done();
      });
    });

    it('should remove team !betwins subcommands', function(done){
      runCommand('team', function(res){
        expect(res).to.have.property('delCommands')
          .that.eql([team1 + 'wins', team2 + 'wins']);
        done();
      });
    });

    function runCommand(betType, cb){
      let cmd = command();
      cmdTester.prepare(cmd, {}, function(ch){
        setStored(ch, betType);
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          cb(res, ch);
        });
      });
    }

    function setStored(ch, betType){
      _.extend(command().getStored(ch), {
        betType: betType,
        teams: betType === 'team'? [team1, team2, 'tie'] : [team1, 'tie'],
        bets: {}
      });
    }
  });

  describe('!betwins', function(){
    const
      team1 = 't1',
      team2 = 't2',
      scoreMult = 1/3;

    function command(){
      return bets.commands.betwins['online.playing'];
    }

    it('should export a "betwins" command', function(){
      expect(bets.commands).to.have.property('betwins')
        .with.property('online.playing');
    });

    it('shoud be authorized to TRUSTED_STAFF users by default', function(){
      expect(command()).to.have.property('auth', ChannelUser.TRUSTED_STAFF);
    });

    it('should export "betwins" and "tiewins" triggers', function(){
      expect(command()).to.have.property('triggers')
        .that.deep.eqls([
          {
            id: 'betwins',
            args: [
              { name: 'team', types: ['string'] },
              { name: 'result', types: ['integer'], optional: true },
              { name: 'bonus', types: ['unsigned'], optional: true }
            ]
          },
          {
            id: 'tiewins',
            aliases: ['draw'],
            aliasCommand: 'betwins tie 0 $bonus',
            args: [
              { name: 'bonus', types: ['unsigned'], optional: true }
            ]
          }
        ]);
    });

    it('should export "winner", "loser" and "tie" messages', function(){
      expect(command()).to.have.property('messages')
        .with.keys('winner', 'loser', 'draw');
    });

    it('should fail to run if called with an invalid team', function(done){
      cmdTester.prepare(command(), { team: 'invalidteam' }, function(ch){
        setStored(ch, 'team');
        cmdTester.run(function(err){
          expect(err).to.have.property('message', 'invalid team name');
          done();
        });
      });
    });

    it('should fail to run if called without a result argument in a score bet', function(done){
      cmdTester.prepare(command(), { team: team1 }, function(ch){
        setStored(ch, 'score');
        cmdTester.run(function(err){
          expect(err).to.have.property('message', 'missing result argument');
          done();
        });
      });
    });

    it('should output a "winner" message on score wins', function(done){
      cmdTester.prepare(command(), { team: team1, result: 3 }, function(ch){
        setStored(ch, 'score');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.property('output')
            .that.eqls({ message: 'winner', team: team1 });
          done();
        });
      });
    });

    it('should output a "winner" message on team wins', function(done){
      cmdTester.prepare(command(), { team: team2 }, function(ch){
        setStored(ch, 'team');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.property('output')
            .that.eqls({ message: 'winner', team: team2 });
          done();
        });
      });
    });

    it('should output a "loser" message on score loses', function(done){
      cmdTester.prepare(command(), { team: team1, result: -3 }, function(ch){
        setStored(ch, 'score');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.property('output')
            .that.eqls({ message: 'loser', team: team1 });
          done();
        });
      });
    });

    it('should output a "draw" message on score ties', function(done){
      cmdTester.prepare(command(), { team: 'tie', result: 0 }, function(ch){
        setStored(ch, 'score');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.deep.property('output.message', 'draw');
          done();
        });
      });
    });

    it('should output a "draw" message on team ties', function(done){
      cmdTester.prepare(command(), { team: 'tie' }, function(ch){
        setStored(ch, 'team');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.deep.property('output.message', 'draw');
          done();
        });
      });
    });

    it('should set the plugin state to "stopped"', function(done){
      cmdTester.prepare(command(), { team: team1 }, function(ch){
        setStored(ch, 'team');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(command().getPluginState(ch)).to.equal('stopped');
          done();
        });
      });
    });

    it('should remove team !betwins subcommands', function(done){
      cmdTester.prepare(command(), { team: team2 }, function(ch){
        setStored(ch, 'team');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.deep.property('delCommands')
            .that.eqls([team1 + 'wins', team2 + 'wins']);
          done();
        });
      });
    });

    it('should remove score !betwins subcommands', function(done){
      cmdTester.prepare(command(), { team: team1, result: 1 }, function(ch){
        setStored(ch, 'score');
        cmdTester.run(function(err, res){
          expect(err).to.be.null;
          expect(res).to.have.deep.property('delCommands')
            .that.eqls([team1 + 'wins', team1 + 'loses', team1 + 'ties']);
          done();
        });
      });
    });

    it('should add coins to team winners', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('team', { team: team2 }, { team: team2, amount: bet, score: 0}, coins, function(coinCount){
        expect(coinCount).to.equal(coins + bet);
        done();
      });
    });

    it('should remove coins from team losers', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('team', { team: team1 }, { team: team2, amount: bet, score: 0}, coins, function(coinCount){
        expect(coinCount).to.equal(coins - bet);
        done();
      });
    });

    it('should add half coins to team losers when tie happens', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('team', { team: 'tie' }, { team: team1, amount: bet, score: 0}, coins, function(coinCount){
        expect(coinCount).to.equal(coins + bet/2);
        done();
      });
    });

    it('should add coins to score winners', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('score', { team: team1, result: 3 }, { team: team1, amount: bet, score: 3}, coins, function(coinCount){
        expect(coinCount).to.equal(coins + bet);
        done();
      });
    });

    it('should add less coins to fuzzy score winners', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('score', { team: team1, result: 3 }, { team: team1, amount: bet, score: 6 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + Math.ceil(1/(1 + (6-3) * scoreMult) * bet));
        done();
      });
    });

    it('should remove coins from score losers', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('score', { team: team1, result: -3 }, { team: team1, amount: bet, score: 3 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + Math.ceil((1/(1 + Math.abs(-3-3) * scoreMult) - 1) * bet));
        done();
      });
    });

    it('should remove more coins from fuzzy score losers', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('score', { team: team1, result: 9 }, { team: team1, amount: bet, score: -3 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + Math.ceil((1/(1 + Math.abs(9-(-3)) * scoreMult) - 1) * bet));
        done();
      });
    });

    it('should add coins to tie score winners', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('score', { team: 'tie' }, { team: 'tie', amount: bet, score: 0 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + bet);
        done();
      });
    });

    it('should remove coins from tie score losers', function(done){
      const
        coins = 100,
        bet = 50;

      coinHelper('score', { team: 'tie' }, { team: team1, amount: bet, score: 3 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + Math.ceil((1/(1 + Math.abs(0-3) * scoreMult) - 1) * bet));
        done();
      });
    });

    it('should apply team bonuses to winners', function(done){
      const
        coins = 100,
        bet = 50,
        bonus = 15;

      coinHelper('team', { team: team1, bonus: bonus },
        { team: team1, amount: bet, score: 0 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + bet + bonus);
        done();
      });
    });

    it('should use result arg as bonus if bonus not present on team bets', function(done){
      const
        coins = 100,
        bet = 50,
        bonus = 15;

      coinHelper('team', { team: team1, result: bonus },
        { team: team1, amount: bet, score: 0 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + bet + bonus);
        done();
      });
    });

    it('should use bonus arg as bonus if bonus and result are both present on team bets', function(done){
      const
        coins = 100,
        bet = 50,
        bonus = 15;

      coinHelper('team', { team: team1, result: 123, bonus: bonus },
        { team: team1, amount: bet, score: 0 }, coins, function(coinCount){
          expect(coinCount).to.equal(coins + bet + bonus);
          done();
        });
    });

    it('should not apply team bonuses to losers', function(done){
      const
        coins = 100,
        bet = 50,
        bonus = 15;

      coinHelper('team', { team: team1, bonus: bonus }, { team: team2, amount: bet, score: 0 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins - bet);
        done();
      });
    });

    it('should apply score bonuses to winners', function(done){
      const
        coins = 100,
        bet = 50,
        bonus = 15;

      coinHelper('score', { team: team1, result: 3, bonus: bonus },
        { team: team1, amount: bet, score: 3 }, coins, function(coinCount){
        expect(coinCount).to.equal(coins + bet + bonus);
        done();
      });
    });

    it('should not apply score bonuses to losers', function(done){
      const
        coins = 100,
        bet = 50,
        bonus = 15;

      coinHelper('score', { team: team1, result: 3, bonus: bonus },
        { team: team1, amount: bet, score: -3 }, coins, function(coinCount){
          expect(coinCount).to.equal(coins + Math.ceil((1/(1 + Math.abs(3-(-3)) * scoreMult) - 1) * bet));
          done();
        });
    });

    function coinHelper(betType, cmdOpts, bet, coins, cb){
      cmdTester.prepare(command(), cmdOpts, function(ch, cmdOpts){
        let bets = {};
        bets[cmdOpts.user.id] = bet;

        setStored(ch, betType, bets);

        async.series([
          function(cb){
            cmdOpts.channelUser.setCurrencyCount('coins', coins, cb);
          },
          function(cb){
            cmdTester.run(cb);
          },
          function(cb){
            cmdOpts.channelUser.getCoins(cb);
          }
        ], function(err, res){
          expect(err).to.not.exist;
          cb(res[res.length-1]);
        });
      });
    }

    function setStored(ch, betType, bets){
      _.extend(command().getStored(ch), {
        betType: betType,
        teams: betType === 'team'? [team1, team2, 'tie'] : [team1, 'tie'],
        bets: bets || {}
      });
    }
  });
});