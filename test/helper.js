"use strict";

const
  _ = require('lodash'),
  conf = require('thebot-conf'),
  redis = conf.redis(),
  db = require('thebot-db')(),
  _chai = require('chai');

conf.log('default', { name: 'core-tests' });

global.chai = _chai;
global.expect = chai.expect;

chai.config.includeStack = true;

global.installDbAndCacheClearer = function(){
  afterEach(global.doDbAndCacheClear);
};

global.doDbAndCacheClear = function(done){
  let
    rllyDone = _.after(5 /*6*/, done),
    d = function(err){
      if(err){
        throw err;
      }
      rllyDone();
    };

  redis.flushdb(d);

  db.Channel.model.remove({}, d);
  db.User.model.remove({}, d);
  db.ChannelUser.model.remove({}, d);
  db.TopList.model.remove({}, d);
  //db.BettingContest.model.remove({}, d);
};