"use strict";

const
  _ = require('lodash'),
  pluginCompiler = require('../lib/plugin-compiler');

describe('Plugin compiler', function(){
  it('should parse single command, single state plugin', function(){
    let p = pluginCompiler({
      name: 'test',
      testCmd: {
        online: {
          exec: _.noop
        }
      }
    });
    
    expect(p).to.have.deep.property('commands.testCmd.online');
  });
  
  it('should parse multi-command, single state plugin', function(){
    let p = pluginCompiler({
      name: 'test',
      testCmd1: {
        online: {
          exec: _.noop
        }
      },
      testCmd2: {
        offline: {
          exec: _.noop
        }
      }
    });
    
    expect(p).to.have.deep.property('commands.testCmd1.online');
    expect(p).to.have.deep.property('commands.testCmd2.offline');
  });
  
  it('should parse single command, 2-level state plugin', function(){
    let p = pluginCompiler({
      name: 'test',
      startingState: 'substate1',
      testCmd: {
        online: {
          exec: _.noop,
          substate1: _.noop,
          substate2: _.noop
        },
        offline: {
          exec: _.noop,
          substate2: _.noop
        }
      }
    });
    
    expect(p).to.have.deep.property('commands.testCmd')
      .with.keys(
        'online.substate1',
        'online.substate2',
        'offline.substate2'
      );
  });
  
  it('should parse dot-notation strings', function(){
    let
      p1 = pluginCompiler({
        name: 'test',
        testCmd: {
          online: { exec: _.noop },
          offline: { exec: _.noop }
        }
      }),
      
      p2 = pluginCompiler({
        name: 'test',
        'testCmd.online.exec': _.noop,
        'testCmd.offline.exec': _.noop
      });
    
    expect(p2.commands).to.deep.eql(p1.commands);
  });
  
  it('should parse leaf commands with inherited exec functions', function(){
    let p = pluginCompiler({
      name: 'test',
      startingState: 'substate1',
      testCmd: {
        exec: _.noop,
        online: {
          before: _.noop
        }
      }
    });
    
    expect(p).to.have.deep.property('commands.testCmd.online.exec');
  });
  
  it('should fail to parse empty plugins', function(){
    expect(pluginCompiler).to.throw(/plugin name/);
    expect(function(){ pluginCompiler({}); }).to.throw(/plugin name/);
  });

  it('should fail to parse unnamed plugins', function(){
    expect(function(){
      pluginCompiler({
        testCmd: {
          online: _.noop
        }
      });
    })
    .to.throw(/plugin name/);
  });
  
  it('should fail to parse zero-command plugins', function(){
    expect(function(){
      pluginCompiler({
        name: 'test'
      });
    })
    .to.throw(/no commands/);
    
    expect(function(){
      pluginCompiler({
        name: 'test',
        testCmd: {}
      });
    })
    .to.throw(/no commands/);
    
    expect(function(){
      pluginCompiler({
        name: 'test',
        testCmd: { exec: _.noop }
      });
    })
    .to.throw(/no commands/);
    
    expect(function(){
      pluginCompiler({
        name: 'test',
        testCmd: { online: { validate: _.noop } }
      });
    })
    .to.throw(/no commands/);
  });
  
  it('should fail to parse top-level defined states', function(){
    expect(function(){
      pluginCompiler({
        name: 'test',
        testCmd: {
          topLevelState: {
            exec: _.noop
          }
        }
      });
    })
    .to.throw(/top\-level states/);
  });
  
  it('should fail to parse missing starting state', function(){
    expect(function(){
      pluginCompiler({
        name: 'test',
        testCmd:{
          online: {
            substate1: _.noop
          }
        }
      });
    })
    .to.throw(/missing starting state/);
  });
  
  it('should fail to parse invalid starting state', function(){
    expect(function(){
      pluginCompiler({
        'name': 'test',
        startingState: 'not a real state',
        testCmd: {
          online: {
            validState: _.noop
          }
        }
      });
    })
    .to.throw(/invalid starting state/);
  });
});

describe('Compiled plugin', function(){
  let p, load, unload, setUp, tearDown, validate, validate2;
  
  beforeEach(function(){
    load=unload=setUp=tearDown=validate=validate2 = null;
  
    p = pluginCompiler({
      name: 'test',
      startingState: 'substate1',
      api: {
        testMethod: function(){ return 'testMethodRet'; }
      },
      privateApi: {
        testPrivateMethod: function(){ return 'testPrivateMethodRet'; }
      },
      load: function(){ load = true; },
      unload: function(){ unload = true; },
      setUp: function(){ setUp = true; },
      tearDown: function(){ tearDown = true; },

      testCmd1: {
        validate: function(){
          validate = true;
        },
        
        online: {
          substate1: {
            exec: function(){ return 'online.substate1.exec'; }
          }
        },
        'offline.substate1': function(){
          return 'offline.substate1.exec';
        }
      },
      
      testCmd2: {
        paused: {
          substate2: {
            exec: function() {
              return 'pause.substate2.exec';
            },
            validate: function(){
              return 'pause.substate2.validate';
            }
          }
        },

        'online.substate1': {
          validate: function(ch, chOpts, callback){
            callback(null, false);
          },
          
          exec: function(ch, chOpts, callback){
            callback(null, 'should not exist');
          }
        }
      },
      
      'testCmd3.online': {
        substate1: {},
        
        validate: function(ch, chOpts, callback){
          let ret = ['validate'];
          callback(null, true, ret);
        },
        before: function(ch, chOpts, callback, ret){
          ret.push('before');
          callback(null, ret);
        },
        exec: function(ch, chOpts, callback, ret){
          ret.push('exec');
          callback(null, ret);
        },
        after: function(ch, chOpts, callback, ret){
          ret.push('after');
          callback(null, ret);
        }
      },
      
      'testCmd4.online.substate1': function(ch, chOpts, callback){
        callback(null, 'online.substate1.exec');
      }
    });
  });
  
  it('should have its defined name', function(){
    expect(p.pluginName).to.equal('test');
  });
  
  it('should have its defined api directly accesible', function(){
    expect(p).to.have.property('testMethod')
      .and.be.a('function');
    expect(p.testMethod()).to.equal('testMethodRet');
  });
  
  it('should have its defined private api directly accessible', function(){
    expect(p).to.have.property('testPrivateMethod')
      .and.be.a('function');
    expect(p.testPrivateMethod()).to.equal('testPrivateMethodRet');
  });
  
  describe('command', function(){
    let fakeChannel,
      fakeOps;
    
    beforeEach(function(){
      fakeChannel = {
        id: '#not-existant',
        commandsKey: 'test:#not-existant:',
        tempStore: {},
        pluginState: {}
      };
      
      fakeOps = { user: {} };
    });
  
    it('should bind command-root validation when no state validation defined', function(){
      p.commands.testCmd1['online.substate1'].validate();
      expect(validate).to.be.true;
    });
    
    it('should bind command validation when defined', function(){
      expect(p.commands.testCmd2['paused.substate2'].validate()).to.equal('pause.substate2.validate');
    });
       
    it('should bind correct functions on different substates', function(){
      expect(p.commands.testCmd1['online.substate1']._exec()).to.equal('online.substate1.exec');
      expect(p.commands.testCmd1['offline.substate1']._exec()).to.equal('offline.substate1.exec');
    });
    
    it('should execute when given a valid channel, user & channelUser', function(done){
      fakeChannel.tempStore.test = { testCmd4: {} };
      p.commands.testCmd4['online.substate1'].exec(fakeChannel, fakeOps, function(err, res){
        expect(err).to.not.exist;
        expect(res).to.equal('online.substate1.exec');
        done();
      });
    });
           
    it('should execute validate, before, exec and after in order', function(done){
      fakeChannel.tempStore.test = { testCmd3: {} };
      p.commands.testCmd3['online.substate1'].exec(fakeChannel, fakeOps, function(err, res){
        expect(err).to.not.exist;
        expect(res).to.eql(['validate', 'before', 'exec', 'after']);
        done();
      });
    });
    
    it('should not execute if validation fails', function(done){
      fakeChannel.tempStore.test = { testCmd2: {} };
      p.commands.testCmd2['online.substate1'].exec(fakeChannel, fakeOps, function(err, res){
        expect(err).to.have.property('message', 'BOT_CMD_NOT_AUTHORIZED');
        expect(res).to.not.exist;
        done();
      });
    });
   
    /*it('should load or create the given user during execution', function(done){
      p.commands.testCmd4['online.substate1'].exec(fakeChannel, fakeOps, function(err, res){
        expect(fakeOps).to.have.property('user')
          .instanceof(User);
        done();
      });
    });*/
  });
});