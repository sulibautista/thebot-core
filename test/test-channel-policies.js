"use strict";

const
  sinon = require('sinon'),
  _ = require('lodash'),
  conf = require('thebot-conf'),
  db = require('thebot-db')(),
  Channel = db.Channel,
  TwitchResolver = db.TwitchResolver,
  ChannelPolicies = require('../lib/channel-policies');

describe('ChannelPolicies', function(){
  let
    channelName,
    opts,
    falseConf,
    trueConf;

  beforeEach(function(){
    channelName = _.uniqueId();
    opts = { resolver: new TwitchResolver() };
    falseConf = {
      get: sinon.stub().withArgs('channel.createIfMissing').returns(false)
    };
    trueConf = {
      get: sinon.stub().withArgs('channel.createIfMissing').returns(true)
    };
  });

  installDbAndCacheClearer();

  describe('#getChannel when config channel.createIfMissing is false', function(){

    it('should get and setup an existing channel from the database', function(done){
      let channelPolicies = new ChannelPolicies(falseConf);
      Channel.createInDatabase(channelName, opts, function(err, dbChannel){
        expect(err).to.be.null;
        channelPolicies.getChannel(channelName, opts, function(err, channel){
          expect(err).to.be.null;
          expect(channel.id).to.eql(dbChannel._id);
          done();
        });
      });
    });

    it('should not create a non existing channel in the database', function(done){
      let channelPolicies = new ChannelPolicies(falseConf);
      channelPolicies.getChannel(channelName, opts, function(err, channel){
        expect(err).to.be.null;
        expect(channel).to.not.exist;
        done();
      });
    });

  });

  describe('#getChannel when config channel.createIfMissing is true', function(){

    it('should get and setup an existing channel from the database', function(done){
      let channelPolicies = new ChannelPolicies(trueConf);
      Channel.createInDatabase(channelName, opts, function(err, dbChannel){
        expect(err).to.be.null;
        channelPolicies.getChannel(dbChannel._id, null, function(err, channel){
          expect(err).to.be.null;
          expect(channel.id).to.eql(dbChannel._id);
          done();
        });
      });
    });

    it('should create and setup a non existing channel in the database', function(done){
      let channelPolicies = new ChannelPolicies(trueConf);
      channelPolicies.getChannel(channelName, opts, function(err, channel){
        expect(err).to.be.null;
        expect(channel).to.have.deep.property('twitch.name', channelName);
        done();
      });
    });

  });

  describe('#getDatabaseChannel when config channel.createIfMissing is false', function(){

    it('should get an existing channel from the database', function(done){
      let channelPolicies = new ChannelPolicies(falseConf);
      Channel.createInDatabase(channelName, opts, function(err, dbChannel1){
        expect(err).to.be.null;
        channelPolicies.getDatabaseChannel(channelName, opts, function(err, dbChannel2){
          expect(err).to.be.null;
          expect(dbChannel1._id).to.eql(dbChannel2._id);
          done();
        });
      });
    });

    it('should not create a non existing channel in the database', function(done){
      let channelPolicies = new ChannelPolicies(falseConf);
      channelPolicies.getDatabaseChannel(channelName, opts, function(err, dbChannel){
        expect(err).to.be.null;
        expect(dbChannel).to.not.exist;
        done();
      });
    });

  });

  describe('#getDatabaseChannel when config channel.createIfMissing is true', function(){

    it('should get an existing channel from the database', function(done){
      let channelPolicies = new ChannelPolicies(trueConf);
      Channel.createInDatabase(channelName, opts, function(err, dbChannel1){
        expect(err).to.be.null;
        channelPolicies.getDatabaseChannel(channelName, opts, function(err, dbChannel2){
          expect(err).to.be.null;
          expect(dbChannel1._id).to.eql(dbChannel2._id);
          done();
        });
      });
    });

    it('should create a non existing channel in the database', function(done){
      let channelPolicies = new ChannelPolicies(trueConf);
      channelPolicies.getDatabaseChannel(channelName, opts, function(err, dbChannel){
        expect(err).to.be.null;
        expect(dbChannel).to.have.deep.property('twitch.name', channelName);
        done();
      });
    });

  });

});