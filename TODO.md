* EXPIRE user keys and possible channels?
    * Users can be cached in redis and expired in one operation, expiration time can be updated each time we fetch it
    from redis, with a multi command.
    * Users come and go regularly, so the expire time should be rather low, maybe around 10-30m, we need to be 
    careful and don't store too many state information the user cache, since it will be delated (see channel below)
    * In the case of channels, we can't expire them unless the channel is being removed completely from the list
    of managed channels, this is because we store a lot of state information for plugins & commands, that should be
    preserved between days.
    * Anyway, the amount of channel related keys is rather low, 1 for the channel, 1 for the pluginState (possibly
    being refactored into the channel later on) and of course all the keys used by the joinQueue, but they don't
    really mather since even if we decide to EXPIRE channels keys, we cant expire them as they JOIN system is detached
    from us

* Load command config on setup

# Design
## App
* Fix disabled commands
* Move PluginCommandWrapper#packResponse to a higher level
* Figure out a way to handle timers
* Figure out a way to get user info, like mods and stuff

### Users

#### Sources
* Users joined can be grabbed from the internal tmi twitch api or by using irc TWITCHCLIENT 1, the api needs polling 
if we want an updated list, the irc does alert us of parts and joins but by using version 1, we cant use version 3,
which has more features like subs notifications and channel specific timeout/info/clear commands. So we probably should
go with api and poll it in a smart way, probably need more IPs to load balance.
* Followers can be grabbed from the twitch api, but polling is required, we can however poll only the latest followers
via limit and orderBy directly in the api, to reduce network strain
* Subscribers can be grabbed from the twitch api, polling is required but we also receive "SPECIALUSER" commands from
irc, so we can use a longer polling interval and also pull whenever we receive such command for a unknown user
* Mods can be grabbed from the internal tmi twitch api, the same endpoint as joined users, polling is required but we 
receive MODE commands from irc, so we can pull whenever we detect an operator change
* Turbo users are not in any api, and can only be recognized via irc incoming "SPECIALUSER" commands, this poses a bit
of a challenge because these commands can be delayed and if we process a turbo-only command it might fail if we haven't
receive a specialuser message for that user, we also can't maintain a list of turbo users for long, because the turbo
status can be revoked at any time from twitch, we have to review this further

#### Design
All viewers have some Twicht metadata allocated for them, even tho at the moment it is mostly irrelevant for the bot
purposes, it will be good to store this data or at least make it easy to store it later on for when we need it for
the viewer's app or any other specific user related stuff. It will also be nice to have for statistics purposes.

So, a global user collection could be implemented, there, we persist all user inherent data. Like displayName (which 
is probably different from chat name), created-at, bio, etc. So all this info is available from twitch directly, we
can't however rely on them solely since we have critical business purposes for this data, so we will store it. Since
the dataset is very large, we need to evict inactive users constantly from the database, inactive users are those
who don't participate in any of the channels the bot is active, for a extended period of time. All of the user's
channel specific state is stored in a UserChannel collection which were we can evict independently, we can therefore,
define some basic eviction rules for bot UserChannels and User:

 * If the user is using one of our free user designated apps, like the website or mobile app, 2-4 moths seems like a 
  reasonable amount of time to keep them. If they paid for anything, they won't be evicted.
 * If all of the user's currency and business critical state in the channel is the default (i.e. 20 coins), evict
 the UserChannel data in 3 days to 1 week
 * If the user has any currency or business critical state the channel, 1 month is probably the maximum retention
  time we can afford, most of the time streamers will reset currency and state weekly at max, by normal bot usage so
  we are going to probably never have inactive users for more than a week anyway, unless bugs happen ofc.
 * If the User has no UserChannel entries and is not a client, evict it immediately.

One advantage of this model is that we can store our client as Users, and simply attach more data to them than we do
for normal users, this has the logical advantage of allowing the exactly same management endpoints for normal 
viewers and clients, and show them different dashboards based on that extra data. A disadvantage tho, is that we need
to be very careful to not evict or otherwise malform the data stored for our clients, since they are paid customers
not like the users. Eventually we hope those users to also be our customers, so this approach makes sense in the long 
rung, not only it will be battle tested when the time comes, but it will make everything easier to manage than say, 
a StreamerCustomer and a ViewerCustomer set of collections.

## Redis
* Figure out a way to handle connection errors
* Investigate a way to reduce roundtrips, currently a minimum of 4 roundtrips are necessary (channel, user, cmd),
hopefully we can reduce this

## Mongo
* Investigate how connection errors are handled
* Ensure all methods that receive a mongo response from a find() operation take into account the possiblity of the
document not existing

## Logging

## Analytics
Use a service like Keen for internal statistics, down the road we might gather more analytics for use in client's
dashboards.

Events:
* Send User session event, record whenever the user enters in chat until he leaves, total = all users * 1
* Command usage stats?