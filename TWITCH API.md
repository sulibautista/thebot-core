# Twitch Api Usage

# Users
For subscriptions and follows we have:

API calls at our disposal:
* List of followed users for channel X, max 100 per batch
* LIst of sub users for channel X, max 100 per batch
* Check if user A follows channel B
* Check if user A subs channel B
* Check all follows/subs for user A

If we have a new channel user, we can get the follow status ASAP from the API, (provided we have the channel permission to do so). If the user is indeed following, we fill in the schema in time.

But, How do we detect unfollows? The API won't tell us this information, so we have to have a background worker checking the follow status. For this task we have 2 API endpoints.

## 1st method
If we use the 'get followed users for channel X' api, we will need to first, accumulate ALL follows in memory
and then do a diff, remove follow for those users not present in the list and add follow to those who dont have it. This is ONLY feasible for small channels, as the number of follows grow, and the number of 'active'
channel users grow, this will need more and more resources to complete, to the point where its going to be impossible to do it fast and periodically.

Ex: Compile follow list from TwitchAPI, for a <100 follow channel this is immediately. Then, find in the database the amount of ChannelUsers that exist for the channel and its FOLLOW status, for a small channel, there will be between 100 to 2k active users, which is not that much considering the amount of information we require. Sort the follow list array, and iterate over the channelUsers array while finding follow differences using binary search on the follower list. All these differences get aggregated into a big message that will be sent to the core to process. Once in the core, it will update all users and invalidate the cache.

Again, this is only possible on small channels, getting a complete list of followers, even for a small <10k follower channel will take a few seconds, if building the list from multiple IPs calling the twitch api. Will take about 2 minutes (assuming no other api call gets thru!) for a single IP.

Another implementation that incrementally updates the follower status is possible. This alleviates some of the problems discussed, since we no longer have to retain all of the users in memory at once, however we introduce complexity and for really big channels, this will still take a while to complete and use a lot of api calls.

## The alternative

... is to do the updates individually per user. Assuming we have a normal, unregistered channel user (i.e. no auth scopes associated), we would need to update her status periodically, using one API call. We wouldn't however update all users with the same priority.

We can use the notion of active users, those who have issued a COMMAND in the past 20-30 minutes. For these active users, we can update their status as follows:

* Every time they enter the 'active' status, i.e. they get loaded from the database into the cache. 
* After X amount of time when a commande is issued for example, 5-30 minutes

By using these two refreshing points, we ensure that a given user status is updated most of the time. In reality, an user that is following or subbing a channel when they _ENTER_ the channel is unlikely to unfollow or unsub during the transmission. And even if they do, is also far less interesting to the streamers and the bot (at the time of the transmission) to know the exact time when user unfollowed or unsubscribed.

The other case, when an active user starts following a channel, is quite common and its also fairly easy to implement by polling the twitch API by follow date. The same is true for subscribers, for these however, we can refresh the user status immediately when we receive a SPECIALUSER (from the IRC server) for a given user that is not a subscriber.

The adventages of this approach are:
* Less time spent being out-of-sync with twitch, for the users that matter
* Its a common rule that the amount of viewers is always less than the amount of followers, so most of the time we will be updating a lot less users than the total amount of followers (which is not true in the 1st method).

Cons:
* Even tho we check far less users, TWITCH api only allows us to check one user-channel follow status per call, so we will be needing  more API calls than the 1st method, unless the amount of active viewers is 1/100 of the followers (Which is unknown at this point)
* This method doesn't take into account updates for offline users. While its true that we don't care about offline users during the transmission, we will be unable to provide correct statistics like "% of followers using the bot".

## A combined approach
Combine both methods to get this:

* (A) New channel users get their status updated right away
    * Covers users that were already follower/subs and then started using the bot
    * Covers users that weren't already follower/subs and then started using the bot

* (B) Returning channel users becoming active (i.e. issuing the first command after a period of inactivity) will get their status updated if it hasn't been updated for some time by (C) or (D).
    * Covers users that were already using the bot, but FOLLOWED/UNFOLLOWED or SUBBED/UNSUBBED hours/minutes before going to interact in the channel

* (C) A background job will be monitoring a channel status for new followers/subscribers, these will be sent as events to the core, only during transmissions.
    * Covers active users that are already using the bot, but FOLLOWED or SUBBED

* (D) Another background job will be incrementally re-checking the user status of all users, updating them in batches
    * Covers offline users that used the bot before, and FOLLOWED/UNFOLLOWED SUBBED/UNSUBBED
    * Covers active users that are already using the bot, UNFOLLOWED or UNSUBBED  

Accuracy at the start of each transmission is 0ms for new users, and the value of max start desync window for returning users. The max start desync window is of X minutes, which can be increased or decreased globally or per channel. This is, the maximum time we allow for results of (C) or (D) (or any other process that updates user state) to be accurate before an user comes active, which should be between 5-60 minutes. 

Once an user is active on a given channel, the desync window for FOLLOWS should be around 3-15 seconds, configurable per channel. The desync window for SUBS should be the minimum of the configured in that channel or the time it takes Twitch IRC server to send an SPECIALUSER message for that user, which in overall should be less than 5 seconds. The desync window for UNFOLLOWs and UNSUBs however, varies a lot and most likely wont be able to be configured explicitly. This is because Twitch doesn't give us an easy way to do this, and iterating over all follow/sub lists is expensive for medium to large channels and will be done incrementally for all channels, that is, we will process batches of users, of different channels simultaneously.

### Implementation

(A) and (B) will share the same implementation, after loading a channel user from the database, we check the last updated time for the interested values (sub & follower), if they are lower than the channel or global threshold, we issue an updated. Once complete, we proceed normally. Notes:

* As of now we don't clear inactive users from cache, so we will compensate this by applying the update logic on each load (even if its from the cache)
* Once this is implemented (with above workaround), the total desync window for SUBS/UNSUBS and FOLLOWS/UNFOLLOWS time will be exactly the value of the treshold user. This allow us to move to production faster *just* with this.


(C) will be implemented as a set of commands in the core:

* !updatetopfollowers Checks the last 100 followers, and adds follower status to those were an existing ChannelUser is present. 
* !updatetopsubs Same as above, but for subs. 
* !setusergroup <user> SUBSCRIBER

The first two will need to be called periodically by either an external program or using our timer API. The third command will be issued from an ircio server, upon receiving a SPECIALUSER for an user via IRC. This will in turn generate an update for the SUBSCRIBER group, and if later on the user unsubs, we should be able to catch that in (A) or (B). However, we can only check for subs on those channels where we have access, for those channels without a proper oauth permission (or config flag), we ignore the request because otherwise the sub status will be enabled forever on that user (unless its removed manually).

For now, only the third command will be implemented. Allowing subs to be detected and use special commands just for them. The first two commands still need some refinement and the timer API to be ready for production.

## Mods
Mod detection is unreliable -sadly-. We have two ways to discover mods:

* Via tmi api
* Via IRC +/-o modes
* Via IRC .mods command

Do this:
    
* Whenever IRC sends us a MODE o or 15s have passed, ask for a .mod list.
* If the mod list is different than the last time or 5 minutes have passed, send a updatemods command to core
* The core will remove all current ChannelUser mods in the database, and add the new mods
* It will also invalidate the caches of the affected users.

## Followers
Follower status will be check via the TwitchAPI every once in a while (see (A)(B)), we won't cache this. If it becomes unavailable, we just use the last known value. Since this adds A LOT of api calls, we have to turn this on specific channels only.

New followers wil be detected in 5-10 seconds windows. These will trigger some events if the streamer wants to. It will also update the follower status on those users who have used the bot in that channel (i.e. existing users).

## Subs
Subs are relatively small in almost all channels. So we can actually maintain a cache table for all subs of a channel, even if they aren't bot users. This will be done transparently via a background job maintaining the cache and sending sub and unsub messages to the core. The core will then execute all necessary notifications and of course, update the sub state in each existing user.

New users will get the sub status from the cache immediately. 

Listening to SPECIALUSER commands in IRC will allow us to detect new subs faster.

## Online Users
We will use TMI for this, but in the future we might want to add a cache layer, although that seems unlikely we will still add an interface to check online users that can be later modified to work with a cache.