"use strict";
const
  async = require('async'),
  ChannelUser = require('thebot-db')().ChannelUser;

module.exports = {
  name: 'commands',
  auth: ChannelUser.TRUSTED_STAFF,

  getallcommands: {
    online: {},
    offline: {},
    paused: {},
    
    exec: function(ch, opts, callback){
      opts.commandService.getCommandTriggers(ch, function(err, triggers){
        callback(err, err? null : {
          allCommands: triggers
        });
      });
    }
  }
};