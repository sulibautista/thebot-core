"use strict";
const
  _ = require('lodash'),
  async = require('async'),

  ChannelUser = require('thebot-db')().ChannelUser,
  User = require('thebot-db')().User;

const
  TOPCOINS_DEFAULT_LIMIT = 10,
  TOPCOINS_MAX_LIMIT = 20;

module.exports = {
  name: 'coins',
      
  'betcoins.online': {
    auth: ChannelUser.NORMAL,
    
    triggers: {
      id: 'betcoins',
      validPrefix: ['#'],
      aliases: ['bc']
    },
    
    messages: {
      count: '@$user $coins BetCoins (#$rank $title)',
      countnotitle: '@$user $coins BetCoins (#$rank)'
    },

    exec: function(ch, opts, callback){
      opts.channelUser.getCurrencyAndRank('coins', function(err, res){
        let
          userRank = res.rank,
          title = null;

        if(ch.coins.ranks){
          ch.coins.ranks.forEach(function(rank){
            if(rank.range.end >= userRank){
              if(rank.range.start <= userRank){
                title = rank.title;
              }
              return false;
            }
          });
        }

        callback(err, err? null : {
          output: {
            message: 'count' + (title? '' : 'notitle'),
            user: opts.user.nick,
            coins: res.count,
            rank: userRank,
            title: title
          }
        });
      });
    }
  },
  
  'topcoins.online': {
    auth: ChannelUser.STAFF,
    
    triggers: [
      {
        id: 'topcoins',
        aliases: ['top10coins'],
        args: [
          { name: 'count', types: ['unsigned'], optional: true }
        ]
      }
    ],
    
    messages: {
      'top': 'BetCoins Top $count: $list'
    },
    
    exec: function(ch, opts, callback){
      let limit = opts.count? Math.min(opts.count, TOPCOINS_MAX_LIMIT) : TOPCOINS_DEFAULT_LIMIT;

      ChannelUser.findTopCurrencyHolders(ch, 'coins', limit, function(err, channelUsers){
        if(err){
          callback(err);
          return;
        }

        let list = _.map(channelUsers, function(channelUser){
          return channelUser.user.twitch.name + ' - ' + channelUser.coins.count;
        });

        callback(null, {
          output:{
            message: 'top',
            count: limit,
            list: list.join(', ')
          }
        });
      });
    }
  },

  'givecoins.online': {
    auth: ChannelUser.TRUSTED_STAFF,
    triggers: [
      {
        id: 'givecoins',
        aliases: ['addcoins', 'addcoin', 'givecoin'],
        args: [
          { name: 'toUser', types: ['string'] },
          { name: 'amount', types: ['integer'] }
        ]
      },
      {
        id: 'takecoins',
        aliases: ['removecoins', 'delcoins', 'takecoin', 'removecoin', 'delcoin'],
        aliasCommand: 'givecoins $toUser -$amount',
        args: [
          { name: 'toUser', types: ['string'] },
          { name: 'amount', types: ['unsigned'] }
        ]
      }
    ],

    messages: {
      'given': '$coins BetCoins given to $user',
      'taken': '$coins BetCoins taken from $user',
      'wronguser': 'Unknown user $user'
    },

    exec: function(ch, opts, callback){
      User.findInDatabase(opts.toUser, opts, function(err, user){
        if(err){
          return callback(err);
        }

        if(user){
          ChannelUser.addCurrency({ channel: ch.id, user: user.id }, ch, 'coins', opts.amount, function(err){
            callback(err, err? null : {
              output: {
                message: opts.amount < 0? 'taken' : 'given',
                coins: Math.abs(opts.amount),
                user: opts.toUser
              }
            });
          });
        } else {
          callback(null, {
            output: {
              message: 'wronguser',
              user: opts.toUser
            }
          });
        }
      });
    }
  },

  'resetallcoins.online': {
    auth: ChannelUser.TRUSTED_STAFF,

    triggers: [
      {
        id: 'resetallcoins',
        aliases: ['resetallbc']
      }
    ],
    
    messages: {
      'reset': 'All user\'s BetCoins have been reset'
    },
    
    exec: function(ch, opts, callback){
      let initCoins = ch.coins.initial;

      ChannelUser.model.where()
        .setOptions({ multi: true })
        .update({ channel: ch.id }, { $set: { 'coins.count': initCoins }}, function(err){
          callback(err, err? null : {
            output: {
              message: 'reset'
            }
          });
        });
    }
  }
};