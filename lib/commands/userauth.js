"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  conf = require('thebot-conf'),
  redis = conf.redis(),
  db = require('thebot-db')(),
  User = db.User,
  ChannelUser = db.ChannelUser;

const
  userAssignableRoles = {
    'normal': ChannelUser.NORMAL,
    'follower': ChannelUser.FOLLOWER,
    'sub': ChannelUser.SUB,
    'subscriber': ChannelUser.SUB,
    'mod': ChannelUser.MOD,
    'moderator': ChannelUser.MOD,
    'trusted_mod': ChannelUser.TRUSTED_MOD,
    'trustedmod': ChannelUser.TRUSTED_MOD,
    'supermod': ChannelUser.TRUSTED_MOD,
    'manager': ChannelUser.MANAGER
  },
  userAssignableRolesArray = Object.keys(userAssignableRoles);

module.exports = {
  name: 'userauth',
  auth: ChannelUser.OWNERS,

  updatemods: {
    triggers: {
      id: 'updatemods',
      args: [
        { name: 'mods', types: ['list{string}'] }
      ]
    },

    online: {},
    offline: {},
    paused: {},

    exec: function(ch, opts, callback){
      /* jshint bitwise: false */
      let
        newModNames = opts.mods,
        newModUserIds;

      async.waterfall([
        function(cb){
          // Get the list of existing mods in the channel so we can delete them before adding the new ones
          ChannelUser.model.find({ channel: ch.id, 'mod.created': { $exists: true } }, {
            _id: 1,
            user: 1
          }, cb);
        },
        function(mods, cb){
          if(mods.length > 0){
            async.parallel([
              function(cb){
                // Revoke existing mod status in the database
                // On failure, the only thing we did was to invalidate some users in the cache,
                // just propagate the error
                ChannelUser.model.where()
                  .setOptions({ multi: true })
                  .update({ _id: { $in: _.pluck(mods, '_id') } }, {
                    $unset: { mod: '' },
                    $bit: { groups: { and: ~ChannelUser.MOD } }
                  })
                  .exec(function(err){ cb(err); });
              },
              function(cb){
                // Invalidate old mods cache
                let ids = mods.map(function(mod){
                  return { channel: ch.id, user: mod.user };
                });

                ChannelUser.cacheDel(redis, ids, function(err){
                  if(err){
                    opts.log(err, '!updatemods - Could not invalidate old MODs in cache, old mods will preserve ' +
                      'mod status until next mod update');
                  }
                  // We failed to invalidate the cache, but we can still add mod status to the new mods
                  cb(null);
                });
              }
            ], function(err){ cb(err); });
          } else {
            cb(null);
          }
        },
        function(cb){
          User.model.find({ 'twitch.name': { $in: newModNames } }, { _id: 1 }, cb);
        },
        function(newMods, cb){
          newModUserIds = _.pluck(newMods, '_id');

          ChannelUser.model.where()
            .setOptions({ multi: true })
            .update({ channel: ch.id, user: { $in: newModUserIds } }, {
              $set: {
                'mod.created':new Date()
              },
              $bit: { groups: { or: ChannelUser.MOD } }
            })
            .exec(function(err){ cb(err); });
        },
        function(cb){
          // New mods are registered, now we just invalidate new mod cache so they obtain the mod status on their
          // next command, if an error occurs we still fail because the new mods won't get noticed until they
          // get invalidated in the next mod update
          let ids = newModUserIds.map(function(id){
            return { channel: ch.id, user: id };
          });

          ChannelUser.cacheDel(redis, ids, cb);
        }
      ], function(err){
        callback(err, err? null : {
          // feedback
        });
      });
    }
  },

  adduserrole: {
    triggers: {
      id: 'adduserrole',
      aliases: ['userrole add'],
      args: [
        { name: 'targetUser', types:['string'] },
        { name: 'roles', types: ['list{string}'] }
      ]
    },

    messages: {
      'rolesupdated': 'Added $roles to $targetUser'
    },

    online: {},
    offline: {},
    paused: {},

    exec: function(ch, opts, callback){
      let roles = _.intersection(_.invoke(opts.roles, 'toLowerCase'), userAssignableRolesArray);

      if(roles.length !== opts.roles.length){
        callback(new Error('one or more invalid user role provided'));
        return;
      }

      let authRoles = roles.reduce(function(prev, next){
        /* jshint bitwise: false */
        return userAssignableRoles[prev] | userAssignableRoles[next];
      }, 0);

      let userId;

      async.waterfall([
        function(cb){
          User.model.findOne({ 'twitch.name': opts.targetUser }, '_id', User.ensureEntity(cb));
        },
        function(user, cb){
          userId = user._id;
          ChannelUser.model.update({ channel: ch.id, user: userId }, {
            $bit: { groups: { or: authRoles } }
          }, cb);
        },
        function(affected, res, cb){
          cb(affected === 1? null : new Error('not existent target channel user'));
        },
        function(cb){
          // The user has been updated, now empty the cache
          ChannelUser.cacheDel(redis, [{ channel: ch.id, user: userId }], cb);
        }
      ], function(err){
        callback(err, err? null : {
          output: {
            message: 'rolesupdated',
            targetUser: opts.targetUser,
            roles: roles.join(', ')
          }
        });
      });
    }
  }
};