"use strict";

module.exports = {
  name: 'info',
  
  info: {
    online: {},
    offline: {},
    paused: {},
    
    triggers: {
      id: 'info'
    },
    
    exec: function(ch, cmdOpts, callback){
      callback(null, {
        output: ch.twitch.name  + ' channel uptime.. unknown >.< - Gr8Bot v0.1.1. You can feel the power.'
      });
    }
  }
};