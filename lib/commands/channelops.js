"use strict";

const
  conf = require('thebot-conf'),
  redis = conf.redis(),
  ChannelPolicies = require('../channel-policies'),
  joinQueue = require('thebot-joinqueue')(redis),
  db = require('thebot-db')(),
  Channel = db.Channel,
  ChannelUser = db.ChannelUser;

let channelPolicies = new ChannelPolicies(conf);
  
module.exports = {
  name: 'channelops',
  auth: ChannelUser.OWNERS,

  api: {
    joinChannel: function(channelId, channelName, opts, callback){
      joinQueue.enqueueJoin(channelId.id, { priority: Date.now() }, function(err){
        if(err) {
          opts.log.warn(err, 'Could not enqueue channel join on channel "%s"', channelName);
          callback(err);
          return;
        }

        callback(null, {
          output: {
            message: 'joining',
            channel: channelName
          }
        });
      });
    }
  },
  
  join: {
    triggers: {
      id: 'join',
      aliases: ['joinchannel'],
      args: [
        { name: 'channelArg', types: ['string'], optional: true }
      ]
    },

    messages: {
      'notjoining': '$channel won\'t be joined',
      'joining': 'Joining $channel...'
    },

    offline: function(ch, opts, callback){
      this.joinChannel(ch.id, ch.twitch.name.toLowerCase(), opts, callback);
    },

    // This command comes from a random online channel, so our clients can issue a join from every
    // channel we are connected to. TODO We need custom auth for this
    online: function(ch, opts, callback){
      let joiningChannel = opts.channelArg;

      if(!joiningChannel){
        callback(new Error('missing channelArg argument'));
        return;
      }

      joiningChannel = joiningChannel.toLowerCase();

      if(joiningChannel[0] !== '#'){
        joiningChannel = '#' + joiningChannel;
      }

      let self = this;
      channelPolicies.getDatabaseChannel(joiningChannel, opts, function(err, channel){
        if(err) {
          callback(err);
        } else if(channel){
          self.joinChannel(channel._id, channel.twitch.name, opts, callback);
        } else {
          callback(null, {
            output: {
              message: 'notjoining',
              channel: joiningChannel
            }
          });
        }
      });
    }
  },

  part: {
    triggers: {
      id: 'part',
      aliases: ['leave', 'partchannel', 'leavechannel']
    },

    messages: {
      'parting': 'Hasta luego!'
    },

    online: function(ch, opts, callback){
      callback(null, {
        part: true,
        output: {
          message: 'parting'
        }
      });
    }
  }
};