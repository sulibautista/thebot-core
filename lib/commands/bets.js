"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  ChannelUser = require('thebot-db')().ChannelUser;

const
  SCORED = 'score',
  TEAM = 'team',
  DEFAULT_ENDBETS_TIMER = 60,
  MAX_ENDBETS_TIMER = 300;

/**
 * The betting contest plugin.
 * @name plugins.bets
 */
module.exports = {
  name: 'bets',
  auth: ChannelUser.TRUSTED_STAFF,
  startingState: 'stopped',

  api: {
    /**
     * Builds a list of !team command triggers for the given teams.
     * @param {array} teams
     */
    getSetBetAliasTriggers: function(teams){
      return _.map(teams, function(team){
        return {
          id: team,
          aliasCommand: 'setbet ' + team + ' $amount $score',
          args: [
            { name:'amount', types:['unsigned', 'set{max}'] },
            { name:'score', types:['integer'], optional: true }
          ]
        };
      });
    },
    
    /**
     * Builds a list of !betwins command's aliases names for the given teams, adding an extra !draw alias for 'tie'.
     * @param {array} teams
     * @param {bool} isScored
     */
    getBetWinsAliases: function(teams, isScored){
      teams = _(teams)
        .without('tie')
        .map(isScored? function(t){
          return _.map(['wins', 'loses', 'ties'], function(suffix){
            return t + suffix;
          });
        } : function(t){
          return t + 'wins';
        });
      
      return (isScored? teams.flatten() : teams).value();
    },
    
    /**
     * Builds a list of !betwins command triggers for the given teams, adding an extra !draw trigger for 'tie'.
     * @param {array} teams
     * @param {bool} isScored
     */
    getBetWinsAliasTriggers: function(teams, isScored){
      let trigger = this.getBetWinsAliasTrigger.bind(this);
      
      teams = _(teams)
        .without('tie')
        .map(isScored? function(t){
          return [
            trigger(t + 'wins', t),
            trigger(t + 'loses', t, true),
            trigger(t + 'ties', 'tie')
          ];
        } : function(t){
          return trigger(t + 'wins', t);
        });
      
      return (isScored? teams.flatten() : teams).value();
    },
    
    getBetWinsAliasTrigger : function(id, team, isReversed){
      let trigger = {
        id: id,
        aliasCommand: 'betwins ' + team + ' $result $bonus',
        args: [
          { name: 'result', types: ['integer'], optional: true },
          { name: 'bonus', types: ['unsigned'], optional: true }
        ]
      };
      
      // Reverse takes a positive result and echoes a negative one to the betwins command
      if(isReversed){
        trigger.aliasCommand = trigger.aliasCommand.replace('$result', '-$result');
        trigger.args[0].types = ['unsigned'];
      }
      
      return trigger;
    }
  },
  
  'betstatus.online': {
    auth: ChannelUser.STAFF,

    triggers: [
      {
        id: 'betstatus'
      }
    ],
    
    messages: {
      stopped: 'There are no betting contests in progress. Use "!teambets <team1> <team2>" or ' +
        '"!scorebets <team1>" to open one. Once started, use "!cancelbets" to cancel the contest',

      teambetting: 'Team betting contest is in progress. Use "#teamOne <amount>", "#teamTwo <amount>" ' +
        'or "#!tie <amount>" to bet. Use "!endbets <seconds>" or "!endbets now" to close the contest',

      scorebetting: 'Score betting contest is in progress. Use "#teamOne <amount> <score>" or ' +
        '"#!tie <amount>" to bet. Use "!endbets <seconds>" or "!endbets now" to close the contest',

      teamplaying: 'Bettings are closed, waiting for the winner. Use "!$team1wins [winners-bonus]", ' +
        '"!$team2wins [winners-bonus]" or "!draw [winners-bonus]" to select the winner. ' +
        'Use "!openbets" to reopen the contest',

      scoreplaying: 'Bettings are closed, waiting for the score. Use "!$team1wins <score> [winners-bonus]", ' +
        '"!$team1loses <score> [winners-bonus]" or "!draw [winners-bonus]" to set the score.' +
        'Use "!openbets" to reopen the contest'
    },
    
    'stopped': function(ch, opts, callback){
      callback(null, {
        output: {
          message: 'stopped'
        }
      });
    },
    
    'betting': function(ch, opts, callback){
      let stored = this.getStored(ch);
      
      callback(null, {
        output: {
          message: stored.betType + 'betting',
          teamOne: stored.teams[0],
          teamTwo: stored.teams[1]
        }
      });
    },
    
    'playing': function(ch, opts, callback){
      let
        stored = this.getStored(ch),
        out = {
          output: {
            message: stored.betType + 'playing'
          }
        };

      _(stored.teams)
        .without('tie')
        .map(stored.betType === SCORED? function(t, i){
          return ['wins', 'loses', 'ties'].map(function(op){
            let entry = {};
            entry['team' + (i+1) + op] = t + op;
            return entry;
          });
        } : function(t, i){
          let entry = {};
          entry['team' + (i+1) + 'wins'] = t + 'wins';
          return entry;
        })
        .flatten()
        .reduce(_.extend, out.output);
      
      callback(null, out);
    }
  },

  'startbets.online.stopped': {
    triggers:[
      {
        id: 'startbets',
        args: [
          { name: 'type', types: ['set{' + SCORED + ',' + TEAM + '}'], optional: true },
          { name: 'teamOne', types: ['string'], optional: true },
          { name: 'teamTwo', types: ['string'], optional: true }
        ]
      },
      {
        id: SCORED + 'bets',
        aliases: ['solobets'],
        aliasCommand: 'startbets ' + SCORED + ' $teamOne',
        args: [
          { name: 'teamOne', types: ['string'], optional:  true }
        ]
      },
      {
        id: TEAM + 'bets',
        aliasCommand: 'startbets ' + TEAM + ' $teamOne $teamTwo',
        args: [
          { name: 'teamOne', types: ['string'], optional:  true },
          { name: 'teamTwo', types: ['string'], optional:  true }
        ]
      }
    ],
    
    messages: {
      scoreplacebets: 'Place your bet on the score! #teamOne coins score (or #!tie)',
      scorebetstarts: '',
        
      teamplacebets: 'Place your bet followed by the coins you are betting! #teamOne vs #teamTwo (or #!tie)',
      teambetstarts: 'Starting new $type bets, viewers can now place bets using #teamOne, #teamTwo or #!tie'
    },
    
    exec: function(ch, opts, callback) {
      let
        stored = this.getStored(ch),
        teams = _.uniq(_.compact([opts.teamOne, opts.teamTwo]));

      if(!opts.type){
        // Try to reuse last run's type
        if(stored.previousRunType){
          opts.type = stored.previousRunType;
        } else{
          callback(new Error('missing type argument'));
          return;
        }
      }

      let isScored = opts.type === SCORED;

      if(teams.length === 0){
        // If there are no teams, we try to reuse last opts.type run's teams
        // we also don't validate them because we already did
        if(stored.previousRuns && stored.previousRuns[opts.type]){
          teams = stored.previousRuns[opts.type].teams;
        } else {
          callback(new Error('wrong team number'));
          return;
        }
      } else {
        // bet is reserved because !betwins command would get shadowed
        if(_.intersection(teams, ['tie', 'bet']).length > 0 ||
          _.any(teams, opts.commandService.commandTriggerExists.bind(opts.commandService, ch))){
          callback(new Error('cannot use team names as commands'));
          return;
        }

        if(isScored){
          // Ignore other possible teams
          teams.length = 1;
        } else if(teams.length < 2){
          callback(new Error('wrong team number'));
          return;
        }

        teams.push('tie');
      }

      _.extend(stored, {
        betType: opts.type,
        previousRunType: opts.type,
        teams: teams,
        bets: {}
      });

      if(!stored.previousRuns){
        stored.previousRuns = {};
      }

      stored.previousRuns[stored.previousRunType] = {
        teams: teams
      };
      
      let self = this;
      async.waterfall([
        this.saveStored.bind(this, ch),
        function(cb){
          self.setPluginState(ch, 'betting');

          let res = { teamOne: teams[0] };
          if(!isScored){
            res.teamTwo = teams[1];
          }
          
          cb(null, {
            feedback: _.extend({
              message: opts.type + 'betstarts',
              type: opts.type }
            , res),
            output: _.extend({
              message: opts.type + 'placebets'
            }, res),
            addCommands: self.getSetBetAliasTriggers(teams)
          });
        }
      ], callback);
    }
  },
  
  'cancelbets.online': {
    triggers: [
      {
        id: 'cancelbets'
      }
    ],
    
    messages: {
      canceled: 'Bets have been canceled'
    },
    
    'betting': {},
    'playing': {},
    
    exec: function(ch, opts, callback){
      let
        state = this.getPluginState(ch),
        stored = this.getStored(ch);
      
      this.setPluginState(ch, 'stopped');
      
      callback(null, {
        output: {
          message: 'canceled'
        },
        // If betting, remove #team commands, otherwise we are playing, remove #teamwins commands
        delCommands: state === 'betting'?
          stored.teams : this.getBetWinsAliases(stored.teams, stored.betType === SCORED)
      });
    }
  },
  
  'setbet.online.betting': {
    auth: ChannelUser.NORMAL,
    
    triggers:[
      {
        id: 'setbet',
        validPrefix: ['#'],
        args: [
          { name: 'team', types: ['string'] },
          { name: 'amount', types: ['unsigned', 'set{max}'] },
          { name: 'score', types: ['integer'], optional: true }
        ]
      }
    ],
    
    messages: {
      
    },
    
    exec: function(ch, opts, callback){
      let stored = this.getStored(ch);

      if(!_.contains(stored.teams, opts.team)){
        callback(new Error('wrong team name'));
        return;
      }
      
      // Score is required for score bets
      if(stored.betType === SCORED && opts.team !== 'tie' && !_.isFinite(opts.score)){
        callback(new Error('missing score argument'));
        return;
      }

      if(opts.amount !== 'max' && opts.amount <= 0){
        callback(new Error('invalid bet amount'));
        return;
      }
      
      let self = this;
      async.waterfall([
        opts.channelUser.getCoins.bind(opts.channelUser),
        function(coins, cb){
          stored.bets[opts.user.id] = {
            team: stored.betType === SCORED && opts.score === 0? 'tie' : opts.team,
            amount: opts.amount === 'max'? coins : Math.min(opts.amount, coins),
            score: opts.score || 0
          };
          self.saveStored(ch, cb);
        }
      ], function(err){
        callback(err, err? null : {
          // TODO feedback
        });
      });
    }
  },
  
  'endbets.online.betting': {
    triggers:[
      {
        id: 'endbets',
        aliases: ['closebets'],
        args: [
          { name: 'time', types: ['unsigned', 'set{now}'], optional: true }
        ]
      }
    ],
    
    messages: {
      endsin: 'Bets end in $time seconds!',
      closed: 'Bets are closed!'
    },
    
    exec: function(ch, opts, callback){
      let stored = this.getStored(ch);
      
      if(opts.time === 'now' || opts.time === 0){
        // If opts.pending end is not present, this is a direct user request. Otherwise we compare the stored
        // pendingEnd with the provided one, if they don't match this operation got cancelled
        if(!opts.pendingEnd || opts.pendingEnd === stored.pendingEnd){
          this.setPluginState(ch, 'playing');

          delete stored.pendingEnd;

          let self = this;
          this.saveStored(ch, function(err){
            callback(err, err? null : {
              output: {
                message: 'closed'
              },
              delCommands: stored.teams,
              addCommands: self.getBetWinsAliasTriggers(stored.teams, stored.betType === SCORED)
            });
          });

        } else {
          callback(new Error('operation got cancelled'));
        }
        return;
      }
      
      let time = opts.time? Math.min(opts.time, MAX_ENDBETS_TIMER) : DEFAULT_ENDBETS_TIMER;

      stored.pendingEnd = _.now() + time*1000;

      let self = this;
      async.waterfall([
        this.saveStored.bind(this, ch),
        function(cb){
          opts.commandService.scheduleCommand(self.createCommand(opts, {
            time: 'now',
            pendingEnd: stored.pendingEnd
          }), time*1000);
          
          cb(null, {
            output: {
              message: 'endsin',
              time: time
            }
          });
        }
      ], callback);
    }
  },
  
  'openbets.online.playing': {
    triggers:[
      {
        id: 'openbets'
      }
    ],
    
    messages: {
      opened: 'The bets have been reopened!'
    },
    
    /**
     * Reopens the bet contest after it was closed by !endbets.
     * Here we just transition into betting state while adding/removing the command aliases for that state.
     */
    exec: function(ch, cmdOpts, callback){
      let stored = this.getStored(ch);
      
      this.setPluginState(ch, 'betting');

      callback(null, {
        output: {
          message: 'opened'
        },
        addCommands: this.getSetBetAliasTriggers(stored.teams),
        delCommands: this.getBetWinsAliases(stored.teams, stored.betType === SCORED)
      });
    }
  },
    
  'betwins.online.playing': {
    triggers:[
      {
        id: 'betwins',
        args: [
          { name: 'team', types: ['string'] },
          { name: 'result', types: ['integer'], optional: true },
          { name: 'bonus', types: ['unsigned'], optional: true }
        ]
      },
      {
        id: 'tiewins',
        aliases: ['draw'],
        aliasCommand: 'betwins tie 0 $bonus',
        args: [
          { name: 'bonus', types: ['unsigned'], optional: true }
        ]
      }
    ],
    
    messages: {
      winner: '$team wins!',
      loser: '$team loses!',
      draw: 'Draw!'
    },
    
    exec: function(ch, cmdOpts, callback){
      let
        stored = this.getStored(ch),
        isScored = stored.betType === SCORED,
        bonus = isScored? (cmdOpts.bonus || 0) : (cmdOpts.bonus ||cmdOpts.result || 0),
        result = isScored && (cmdOpts.team === 'tie'? 0 : cmdOpts.result),
        winner = result === 0? 'tie' : cmdOpts.team;
        
      if(!_.contains(stored.teams, winner)){
        callback(new Error('invalid team name'));
        return;
      }
      
      if(isScored && !_.isFinite(result)){
        callback(new Error('missing result argument'));
        return;
      }
      
      let coinChanges = [];
      
      _.forOwn(stored.bets, function(bet, userId){
        let delta = 0;
        
        if(isScored){
          let
            dist = Math.abs(result - bet.score),
            mult = dist > 0? 1 / (1 + dist * (1/3)) : 1;
          
          // Lose
          if(bet.team !== winner || (result < 0 && bet.score > 0) || (result > 0 && bet.score < 0)){
            mult = mult - 1;
          } else {
            // Add bonus just for the winners
            delta = bonus;
          }
          
          delta += bet.amount * mult;
        } else {
          delta = bet.team === winner? bet.amount + bonus :
              (winner === 'tie'? bet.amount/2 : -bet.amount);
        }
        
        delta = Math.ceil(delta);
        
        if(delta !== 0){
          coinChanges.push({
            userId: userId,
            delta: delta
          });
        }
      });
      
      let
        self = this,
        resCallback = function(err){
          if(err){
            callback(err);
            return;
          }
                    
          self.setPluginState(ch, 'stopped');
          
          callback(null, {
            output: {
              message: winner === 'tie'? 'draw' : (isScored && result < 0? 'loser' : 'winner'),
              team: winner
            },
            delCommands: self.getBetWinsAliases(stored.teams, isScored)
          });
        };
      
      if(coinChanges.length > 0){
        async.each(coinChanges, function(cc, done){
          ChannelUser.addCurrency({ channel: ch.id, user: cc.userId }, ch, 'coins', cc.delta, done);
        }, resCallback);
      } else {
        resCallback();
      }
    }
  }
};