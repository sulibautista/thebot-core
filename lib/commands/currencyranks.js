"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  Channel = require('thebot-db')().Channel,
  ChannelUser = require('thebot-db')().ChannelUser;

module.exports = {
  name: 'currencyranks',
  auth: ChannelUser.TRUSTED_STAFF,

  api: {
    addRank: function(ch, opts){
      let
        title = opts.title,
        currency = opts.currency,
        range = opts.range,
        i = 0,
        oldRanks = ch[currency].ranks || [],
        len = oldRanks.length,
        newRanks = [];

      for(; i < len; ++i){
        let oldRange = oldRanks[i].range;

        if(oldRange.end < range.start){
          newRanks.push(oldRanks[i]);
        } else {
          if(oldRange.start < range.start){
            oldRange.end = range.start-1;
            newRanks.push(oldRanks[i]);
            ++i;
          } else if(oldRange.start === range.start){
            if(oldRange.start !== oldRange.end){
              ++oldRange.start;
            } else {
              ++i;
            }
          }

          break;
        }
      }

      for(; i < len; ++i){
        let oldRange = oldRanks[i].range;

        if(oldRange.end > range.end){
          if(oldRange.start <= range.end) {
            oldRange.start = range.end + 1;
          }
          break;
        }
      }

      let pos = -1 + newRanks.push({
        range: range,
        title: title
      });

      for(; i < len; ++i){
        newRanks.push(oldRanks[i]);
      }

      ch[currency].ranks = newRanks;
      ch.dirty();

      return {
        pos: pos,
        ranks: newRanks
      };
    }
  },

  'setrank.online': {
    triggers: [
      {
        id: 'setrank',
        aliases: ['addrank'],
        args: [
          { name: 'currency', types:['string'] },
          { name: 'range', types: ['range', 'unsigned'] },
          { name: 'title', types: ['string'] }
        ]
      },
      {
        id: 'setcoinrank',
        aliases: ['setcoinsrank', 'addcoinrank', 'addcoinsrank'],
        aliasCommand: 'setrank coins $range $title',
        args: [
          { name: 'range', types: ['range', 'unsigned'] },
          { name: 'title', types: ['string'] }
        ]
      }
    ],

    messages: {
      titleset: 'Viewers at rank $start are now titled $title',
      titleset_n: 'Viewers between ranks $start and $end (inclusive) are now titled $title'
    },

    exec: function(ch, opts, callback){
      if(!_.contains(['coins', 'points'], opts.currency)){
        callback(new Error('unknown currency'));
        return;
      }

      if(_.isFinite(opts.range)){
        opts.range = { start: opts.range, end: opts.range };
      }

      let res = this.addRank(ch, opts);

      let $set = {};
      $set[opts.currency + '.ranks'] = res.ranks;

      Channel.model.update({ _id: ch.id }, { $set: $set }, function(err){
        callback(err, err? null : {
          output: {
            message: 'titleset' + (opts.range.start === opts.range.end? '' : '_n'),
            title: opts.title,
            start: opts.range.start,
            end: opts.range.end
          }
        });
      });
    }
  },

  'delrank.online': {
    triggers: [
      {
        id: 'delrank',
        args: [
          { name: 'currency', types:['string'] },
          { name: 'range', types: ['range', 'unsigned'] }
        ]
      },
      {
        id: 'delcoinrank',
        aliases: ['delcoinranks', 'delcoinsrank', 'delcoinsranks'],
        aliasCommand: 'delrank coins $range',
        args: [
          { name: 'range', types: ['range', 'unsigned'] }
        ]
      }
    ],

    messages: {
      titledel: 'Removed rank #$start title',
      titledel_n: 'Removed rank titles for ranks $start-$end'
    },

    exec: function(ch, opts, callback){
      if(!_.contains(['coins', 'points'], opts.currency)){
        callback(new Error('unknown currency'));
        return;
      }

      if(_.isFinite(opts.range)){
        opts.range = { start: opts.range, end: opts.range };
      }

      // Add a sentinel title so it doesn't get 'merged' into another range
      opts.title = ' ';

      let res = this.addRank(ch, opts);
      console.log(res);
      // Adding a rank will remove/cut existing ranks in the specified range, so we do that and then just
      // remove the newly inserted rank, effectively removing all ranks between start and end
      ch[opts.currency].ranks.splice(res.pos, 1);

      let $set = {};
      $set[opts.currency + '.ranks'] = res.ranks;

      Channel.model.update({ _id: ch.id }, { $set: $set }, function(err){
        callback(err, err? null : {
          output: {
            message: 'titledel' + (opts.range.start === opts.range.end? '' : '_n'),
            start: opts.range.start,
            end: opts.range.end
          }
        });
      });
    }
  },

  'showranks.online': {
    auth: ChannelUser.STAFF,

    triggers: [
      {
        id: 'showranks',
        args: [
          { name: 'currency', types: ['string'] }
        ]
      },
      {
        id: 'showcoinranks',
        aliases: ['showcoinrank', 'showcoinsranks', 'showcoinsrank'],
        aliasCommand: 'showranks coins'
      }
    ],

    messages: {
      'noranks': 'There currently no ranks',
      'ranks': 'Ranks: $ranks'
    },

    exec: function(ch, opts, callback){
      if(!_.contains(['coins', 'points'], opts.currency)){
        callback(new Error('unknown currency'));
        return;
      }

      let ranks = ch[opts.currency].ranks;

      if(ranks && ranks.length > 0){
        ranks = ranks.map(function(rank){
          let range = rank.range;
          return (range.start === range.end? range.start : range.start + '-' + range.end) + ' ' + rank.title;
        }).join(', ');

        callback(null, {
          output: {
            message: 'ranks',
            ranks: ranks
          }
        });
      } else {
        callback(null, {
          output: {
            message: 'noranks'
          }
        });
      }
    }
  }
};