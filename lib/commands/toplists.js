"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  
  sdb = require('thebot-db')(),
  User = sdb.User,
  ChannelUser = sdb.ChannelUser,
  TopList = sdb.TopList;

const
  MAX_RANGE = 100;

module.exports = {
  name: 'toplists',
  auth: ChannelUser.TRUSTED_STAFF,
  startingState: 'stopped',
  
  api: {
    checkRange: function(range){
      range.start = Math.max(1, Math.min(range.start, MAX_RANGE-1));
      range.end = Math.max(1, Math.min(range.end, MAX_RANGE));
      
      return range.start < range.end;
    }/*,
    
    findUserInRange: function(){
      
    }*/
  },
  
  'starttoplist.online.stopped': {
    triggers:[
      {
        id: 'starttoplist',
        aliases: ['tournament start'],
        args: [
          { name: 'ranges', types: ['list{range}']}
        ]
      }
    ],
    
    messages: {
      'started': 'The tournament has started'
    },
    
    exec: function(ch, opts, callback){
      for(let i = 0, len = opts.ranges.length; i < len; ++i){
        if(!this.checkRange(opts.ranges[i])){
          callback(new Error('invalid range'));
          return;
        }
      }
      
      this.setPluginState(ch, 'started');
      
      TopList.model.create({
        channel: ch.id,
        ranges: opts.ranges
      }, function(err){
        callback(err, err? null : {
          output: {
            message: 'started'
          }
        });
      });
    }
  },
  
  'snapshottoplist.online.started': {
    triggers:[
      {
        id: 'snapshottoplist',
        aliases: ['tournament give tickets'],
        args: [
          { name: 'noreset', types: ['set{noreset}'], optional: true }
        ]
      }
    ],
    
    messages: {
      'snapshot': 'All tickets given',
      'snapshotreset': 'All tickets given, all BetCoins reseted'
    },
    
    exec: function(ch, opts, callback){
      let reset = !opts.noreset;
      
      async.waterfall([
        function(cb){
          ChannelUser.model.find()
            .select('user')
            .where('channel').equals(ch.id)
            .sort('-coins.count')
            .limit(MAX_RANGE)
            .exec(cb);
        },
        function(chUsers, cb){
          let
            snapshot = _.pluck(chUsers, 'user'),
            steps = {};
          
          steps.pushSnapshot = function(cb){
            async.waterfall([
              TopList.getLastActive.bind(TopList, ch),
              function(topList, cb){
                topList.snapshots.push(snapshot);
                topList.save(cb);
              }
            ], cb);
          };
          
          if(reset){
            steps.resetCoins = function(cb){
              ChannelUser.model.where()
                .setOptions({ multi: true })
                .update({ channel: ch.id }, { $set: { 'coins.count': ch.coins.initial }}, cb);
            };
          }
          
          async.parallel(steps, cb);
        },
        function(res, cb){
          cb(null, {
            output: {
              message: 'snapshot' + (reset? 'reset' : '')
            }
          });
        }
      ], callback);
    }
  },
  
  'checktoplist.online.started': {
    auth: ChannelUser.NORMAL,
    
    triggers:[
      {
        id: 'checktoplist',
        aliases: ['tickets', 'ticket']
      }
    ],
    
    messages: {
      'entries': '@$user Tickets: $entries',
      'noentries': '@$user No tickets'
    },
    
    exec: function(ch, opts, callback){
      async.waterfall([
        TopList.getLastActive.bind(TopList, ch),
        function(topList, cb){
          let
            entries = [],
            userId = opts.user.id;
          
          topList.ranges.forEach(function(range){
            let count = 0;
            
            topList.snapshots.forEach(function(users){
              for(let i = range.start-1, end = range.end, len = users.length; i < len && i < end; ++i){
                if(userId.equals(users[i])){
                  ++count;
                }
              }
            });
            
            if(count > 0) {
              entries.push(count + ' ' + '(' + range.start + '-' + range.end + ')');
            }
          });
                    
          cb(null, {
            output: {
              message: (entries.length? '' : 'no') + 'entries',
              user: opts.user.nick,
              entries: entries.join(', ')
            }
          });
        }
      ], callback);
    }
  },
  
  'randomtoplist.online.started': {
    triggers:[
      {
        id: 'randomtoplist',
        aliases: ['tournament random', 'tournament pick random'],
        args: [
          { name: 'range', types: ['range'] }
        ]
      }
    ],
    
    messages: {
      'random': 'And the winner is...  $user!',
      'noentries': 'There are no user\'s with tickets currently'
    },
    
    exec: function(ch, opts, callback){
      let self = this;
      async.waterfall([
        TopList.getLastActive.bind(TopList, ch),
        function(topList, cb){
          let
            range = opts.range,
            entries;
            
          if(!self.checkRange(opts.range)){
            cb(new Error('invalid range'));
            return;
          }
          
          entries = _.reduce(topList.snapshots, function(e, users){
            for(let i = range.start-1, end = range.end, len = users.length; i < len && i < end; ++i){
              if(users[i]){
                e.push(users[i]);
              }
            }
            return e;
          }, []);
          
          if(entries.length > 0){
            async.retry(10, function(cb){
              User.model.findById(entries[_.random(0, entries.length)], 'twitch.name', function(err, user){
                cb(err || !user? new Error('user not found') : null, user);
              });
            }, cb);
          } else {
            cb(null, null);
          }
        },
        function(user, cb){
          cb(null, {
            output:{
              message: user? 'random' : 'noentries',
              user: user && user.twitch.name
            }
          });
        }
      ], callback);
    }
  },
  
  'endtoplist.online.started': {
    triggers:[
      {
        id: 'endtoplist',
        aliases: ['tournament end']
      }
    ],
    
    messages: {
      'ended': 'The tournament has ended',
      'notstarted': 'There is no tournament to close'
    },
    
    exec: function(ch, opts, callback){
      this.setPluginState(ch, 'stopped');
            
      async.waterfall([
        function(cb){
          TopList.getActiveQuery(ch)
            .select('ended')
            .exec(cb);
        },
        function(topList, cb){
          if(topList){
            topList.ended = new Date();
            topList.save(function(err){ cb(err, topList); });
          } else {
            cb(null, null);
          }
        },
        function(topList, cb){
          cb(null, {
            output: {
              message: topList? 'ended' : 'notstarted'
            }
          });
        }
      ], callback);
    }
  }
};