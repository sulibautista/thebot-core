"use strict";

const
  Channel = require('thebot-db')().Channel;

module.exports = ChannelPolicies;

function ChannelPolicies(conf){
  if(conf.get('channel.createIfMissing')){
    this.getChannel = function(idOrName, channelArgs, cb){
      Channel.findInDatabaseOrCreateAndSetUp(idOrName, channelArgs, channelArgs, cb);
    };

    this.getDatabaseChannel = function(idOrName, channelArgs, cb){
      Channel.findInDatabaseOrCreate(idOrName, channelArgs, channelArgs, cb);
    };

  } else {
    this.getChannel = Channel.findInDatabaseAndSetUp.bind(Channel);

    this.getDatabaseChannel = Channel.findInDatabase.bind(Channel);
  }
}



