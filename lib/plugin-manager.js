"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  fs = require('fs'),
  
  conf = require('thebot-conf'),
  log = conf.log(),
  redis = conf.redis(),
  pluginCompiler = require('./plugin-compiler.js');
  
module.exports = PluginManager;

function PluginManager(){
  this.plugins = {};
  this.commands = {};
  this.cmdPluginMap = {};
  this.loaded = {};
}

PluginManager.CMD_INVALID_STATE = 1;
PluginManager.CMD_NOT_LOADED = 3;

/**
 * Loads all plugins in the specified directory.
 * 
 */
PluginManager.prototype.loadPlugins = function(dir, callback){
  if(this.loaded[dir]){
    callback();
  } else {
    let self = this;
    async.waterfall([
      this._loadPluginsDir.bind(this, dir),
      function(plugins, cb){
        plugins.forEach(self.installPlugin, self);
        self.loaded[dir] = true;
        cb(null);
      }
    ], callback);
  }
};

PluginManager.prototype.installPlugin = function(plugin){
  this.plugins[plugin.pluginName] = plugin;
  
  _.forOwn(plugin.commands, function(cmd, cmdName){
    // Aggregate all commands for fast retrieval
    this.commands[cmdName] = cmd;
    
    // We also need to know what commands belongs to what plugin, so we can retrieve state from the channel
    this.cmdPluginMap[cmdName] = plugin;
  }, this);
};

// TODO add to this.loaded, also private plugins?
PluginManager.prototype.loadPluginFile = function(file, callback){
  try {
    let plugin = pluginCompiler(require(file));
    this.installPlugin(plugin);
    callback(null, plugin);
  } catch(e){
    log.error(e, 'An exception ocurred while loading plugin in file %s', file);
    callback(e);
  }
};

PluginManager.prototype.isKnownCommand = function(cmdId){
  return !!this.commands.hasOwnProperty(cmdId);
};

/**
 * Setup loaded plugins in the given channel.
 * @param dbChannel
 * @param channel
 * @param callback
 */
PluginManager.prototype.setUpCommands = function(dbChannel, channel, callback){
  let initSettings = {};
  _.forOwn(this.plugins, function(plugin){
    _.extend(initSettings, plugin.setUp(dbChannel, channel));
  });

  channel.loadedPlugins.sort();
  
  redis.hmset(channel.getCommandsKey(), initSettings, callback);
};

PluginManager.prototype.setUpCommand = function(cmdId, channel, callback){
  let plugin = this.cmdPluginMap[cmdId];
  plugin.setUpOnLoaded(channel, callback);
};

/**
 * Retrieves the requested command id for the given channel. 
 * The returned command will be correct only for the given channel's state. 
 */
PluginManager.prototype.getCommand = function(cmdId, channel){
  let
    pluginName = this.cmdPluginMap[cmdId].pluginName,
    pluginLoaded = _.indexOf(channel.loadedPlugins, pluginName, true);

  if(pluginLoaded !== -1){
    let pluginState = channel.pluginState[pluginName];
    return this.commands[cmdId][pluginState? (channel.state + '.' + pluginState) : channel.state] ||
      PluginManager.CMD_INVALID_STATE;
  } else {
    return PluginManager.CMD_NOT_LOADED;
  }
};

/**
 * Builds an array containing all the command triggers, both fixed and temporal, of a channel's non-disabled commands.
 * TODO disabled commands, maybe load whole command state hash from redis? or use persistent channel.disabledCmds
 */
PluginManager.prototype.getCommandTriggers = function(channel, callback){
  let triggers = channel.tempCommands.slice(0);

  _.forOwn(this.plugins, function(plugin){
    triggers.push.apply(triggers, plugin.getCommandTriggers());
  });

  callback(null, triggers);
};

/**
 * Loads all the plugins in the given dir.
 */
PluginManager.prototype._loadPluginsDir = function(dir, callback){
  fs.exists(dir, function(exist){
    if(!exist){
      callback(new Error('path doesn\'t exist'));
    }
  });
  
  fs.readdir(dir, function(err, files){
    if(err){
      return callback(err);
    }
    
    let plugins = [];
    files.forEach(function(file){
      if(file.match(/.+\.js/g) !== null && file !== 'index.js'){
        try {
          let plugin = pluginCompiler(require(dir + '/' + file));
          plugins.push(plugin);
        } catch(e){
          log.error(e, 'An exception occurred while loading plugin "%s/%s"', dir, file);
        }
      }
    });

    log.info('%d plugins loaded from "%s"', plugins.length, dir);
    
    return callback(null, plugins);
  });
};