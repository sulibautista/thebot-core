/**
 * Compiles command plugin definitions.
 * @file
 * @todo Use Object.setPrototypeOf() instead of __proto___. Better yet, refactor this to a different implementation
 * where we don't need to deal with prototypes.
 */
"use strict";

/* jshint proto: true */

const
  _ = require('lodash'),
  
  PluginCommandWrapper = require('./plugin-command-wrapper.js');

const generalParser = {
  // A custom parser. It is directly accessed by the command and plugin parsers, nothing to do here.
  'parser': _.noop,
  
  'auth': function(auth){
    if(!_.isNumber(auth)){ throw new Error('auth value is not a number'); }
    this.auth = auth;
  },
  
  'validate': function(fn){
    if(!_.isFunction(fn)){ throw new Error('validate value is not a function'); }
    this.validate = fn;
  },
  
  'exec': function(fn){
    if(!_.isFunction(fn)){ throw new Error('exec value is not a function'); }
    this._exec = fn;
  },
  
  'before': function(fn){
    if(!_.isFunction(fn)){ throw new Error('before value is not a function'); }
    this.before = fn;
  },
  
  'after': function(fn){
    if(!_.isFunction(fn)){ throw new Error('after value is not a function'); }
    this.after = fn;
  }
};

const pluginAttrParser = {
  'name': function(name){
    if(!_.isString(name)){ throw new Error('plugin.name value is not a string'); }
    this.pluginName = name;
  },
  'startingState': function(s){
    if(!_.isString(s)){ throw new Error('plugin.startingState value is not a string'); }
    this.startingState = s;
  },
  
  'api': function(fnMap){
    if(!_.isPlainObject(fnMap)){ throw new Error('plugin.api value is not an object'); }
    _.merge(this, fnMap);
  },
  
  'privateApi': function(fnMap){
    if(!_.isPlainObject(fnMap)){ throw new Error('plugin.privateApi value is not an object'); }
    _.merge(this, fnMap);
  },
  
  'load': function(fn){
    if(!_.isFunction(fn)){ throw new Error('plugin.load value is not a function'); }
    pushCallback(this, '_onLoad', fn);
  },
  
  'unload': function(fn){
    if(!_.isFunction(fn)){ throw new Error('plugin.unload value is not a function'); }
    pushCallback(this, '_onUnload', fn);
  },
  
  'setUp': function(fn){
    if(!_.isFunction(fn)){ throw new Error('plugin.setUp value is not a function'); }
    pushCallback(this, '_onSetUp', fn);
  },
  
  'tearDown': function(fn){
    if(!_.isFunction(fn)){ throw new Error('plugin.tearDown value is not a function'); }
    pushCallback(this, '_onTearDown', fn);
  }
};

const cmdAttrParser = {
  'triggers': function(triggers){
    if(!_.isPlainObject(triggers) && !_.isArray(triggers)){
      throw new Error('command.triggers value is not an object');
    }
    this.triggers = _.isArray(triggers)? triggers : [triggers];
  },
  
  'messages': function(messages){
    if(!_.isPlainObject(messages)){ throw new Error('command.messages value is not an object'); }
    this.messages = messages;
  }
};

pluginAttrParser.__proto__ = generalParser;
cmdAttrParser.__proto__ = generalParser;

module.exports = function(def){
  if(!def || !def.name) {
    throw new Error('missing plugin name');
  }

  let
    plugin = { def: def },
    states = { 0: {online: true, offline: true, paused: true} },
    commands = {},
    wrapper = PluginCommandWrapper.prototype;
  
  if(def.wrapper){
    def.wrapper.__proto__ = PluginCommandWrapper.prototype;
    wrapper = def.wrapper;
    delete def.wrapper;
  }

  plugin.__proto__ = wrapper;
  unpack(def);
  
  _.forOwn(def, function(value, key){
    let parser = pluginAttrParser[key];
    
    // Is it a known prop?
    if(parser){
      parser.call(plugin, value);
    } else {
      // Assume is a command
      let cmd = parseCommand(value, {}, states, 0);
      if(cmd){
        cmd.__proto__ = plugin;
        commands[key] = cmd;
      }
    }
  });
  
  if(states[1]){
    if(!def.startingState){
      throw new Error('missing starting state');
    }
      
    if(!_.has(states[1], def.startingState)){
      throw new Error('invalid starting state');
    }
  }
  
  if(_.keys(commands).length === 0 && validateLeafCommand(plugin)){
    commands[def.name] = plugin;
  }
  
  let
    cmdHash = {},
    cmdIds = [];
    
  genStatePermutations(states).forEach(function(statePerm){
    _.forOwn(commands, function(cmd, cmdId){
      let child = cmd;
      
      for(let i = 0, len = statePerm.length; i < len; ++i){
        if(child._children && child._children[statePerm[i]]){
          child = child._children[statePerm[i]];
        } else {
          return;
        }
      }
      
      if(!cmdHash[cmdId]){
        cmdHash[cmdId] = {};
      }
      
      cmdHash[cmdId][statePerm.join('.')] = child;
      child.id = cmdId;
    });
  });
  
  if(_.isEmpty(cmdHash)){
    throw new Error('no commands defined');
  }
  
  plugin.commands = cmdHash;
  return plugin;
};
  
function unpack(obj){
  _.forOwn(obj, function(value, key){
    let keys = key.split('.');
    if(keys.length > 1){
      let o = obj, end = keys.length-1;
      for(let i = 0; i < end; ++i){
        let k = keys[i];
        if(!o[k]){
          o[k] = {};
        }
        o = o[k];
      }
      o[keys[end]] = value;
      delete obj[key];
    }
    
    if(_.isPlainObject(value)){
      unpack(value);
    }
  });
}

function parseCommand(opts, ancestor, states, level){
  let c = {};
    
  if(_.isFunction(opts)){
    ((c.parser && c.parser.exec) || cmdAttrParser.exec).call(c, opts);
  } else {
    let childCommands = {};
    
    _.forOwn(opts, function(value, key){
      let parser = (c.parser && c.parser[key]) || cmdAttrParser[key];
      if(parser){
        parser.call(c, value);
      } else {
        // Not a property? Key must be a state and value a child command, queue it
        childCommands[key] = value;
      }
    });
    
    // We parse children after so all of our properties get propagated to the prototypical chain
    _.forOwn(childCommands, function(value, key){
      let cmd = parseCommand(value, c, states, level+1);
      if(cmd) {
        if(!c._children){
          c._children = {};
        }
        c._children[key] = cmd;
      }
      
      if(!states[level]) {
        states[level] = {};
      }
        
      if(level === 0 && !states[0][key]){
        throw new Error('defining new top-level states is not supported');
      }
      states[level][key] = true;
    });
  }
  
  c.__proto__ = ancestor;
  return c._children? c : validateLeafCommand(c);
}

function validateLeafCommand(cmd){
  // Leaf commands *need* an exec command, since they have no children who can use what this command inherits
  if(!cmd._exec){
    return;
  }
  
  return cmd;
}

function genStatePermutations(states){
  let arr = _.toArray(states);
  arr.forEach(function(state, k){
    arr[k] =  _.keys(state);
  });
  return getCombinations(arr, arr.length);
}

function getCombinations(arr, n){
  var i,j,k,elem,l = arr.length,childperm,ret=[];
  if(n === 1){
    for(i = 0; i < arr.length; i++){
      for(j = 0; j < arr[i].length; j++){
        ret.push([arr[i][j]]);
      }
    }
    return ret;
  }
  else{
    for(i = 0; i < l; i++){
      elem = arr.shift();
      for(j = 0; j < elem.length; j++){
        childperm = getCombinations(arr.slice(), n-1);
        for(k = 0; k < childperm.length; k++){
          ret.push([elem[j]].concat(childperm[k]));
        }
      }
    }
    return ret;
  }
}

function pushCallback(cmd, scope, fn){
  (cmd[scope] || (cmd[scope] = [])).push(fn);
}