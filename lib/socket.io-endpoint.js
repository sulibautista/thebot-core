"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  io = require('socket.io')(),
  forwarded = require('forwarded-for'),
  
  conf = require('thebot-conf'),
  log = conf.log();

module.exports = SocketIoEndpoint;

function SocketIoEndpoint(opts){
  this.commandService = opts.commandService;
  this.port = opts.port;
}

const cmdRequiredFields = ['id', 'channelService', 'userName', 'channelName', 'userId', 'channelId'];

SocketIoEndpoint.prototype.start = function(){
  log.info('Starting socket.io endpoint');
  
  let self = this;
  io.on('connection', function(socket){
    socket.ip = forwarded(socket.request, socket.request.headers).ip;
    log.info('New socket.io connection from %s', socket.ip);
    self.addSocketListeners(socket);
  });
  
  io.listen(this.port);
};

SocketIoEndpoint.prototype.addSocketListeners = function(socket){
  let self = this;
  
  socket.on('command', function(cmd) {
    cmd.args = _.omit(cmd, cmdRequiredFields);

    self.commandService.execCommand(cmd, function(err, res){
      if(err){
        /*if(err.name === 'BOT_CMD_INVALID'){
          self.abortClient(socket, 'Invalid command from %s, aborting connection', socket.ip, cmd);
        }*/
        if(cmd && cmd.log){
          cmd.log.warn(err, 'Socket.io endpoint command error');
        } else {
          log.warn(err, 'Socket.io endpoint command error', {creq: cmd});
        }
      } else {
        cmd.log.info({ cres: res}, 'Socket.io endpoint command executed');
      }
    });
  });
};

SocketIoEndpoint.prototype.abortClient = function(socket){
  if(arguments.length > 1){
    log.warn.apply(log, Array.prototype.slice.call(arguments, 1));
  }
  
  socket.disconnect();
};