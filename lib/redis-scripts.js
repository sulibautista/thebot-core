"use strict";

let scripts = {};

module.exports = scripts;

/* jshint multistr:true */

scripts.get_Channel_User_ChannelUser_fromIdId =
"local \
  channelKey, \
  userKey, \
  channelUserKey, \
  joinStatusKey, \
    \
  channelId, \
  userId =  \
  \
  KEYS[1], \
  KEYS[2], \
  KEYS[3], \
  KEYS[4], \
  \
  ARGV[1], \
  ARGV[2] \
\
local res = { \
  ['channel'] = redis.call('hget', channelKey, channelId), \
  ['user'] = redis.call('hget', userKey, userId), \
  ['channelUser'] = redis.call('hget', channelUserKey, userId), \
  ['joinStatus'] = redis.call('hget', joinStatusKey, channelId), \
} \
return cjson.encode(res)";

scripts.get_Channel_User_ChannelUser_fromIdName =
"local \
  channelKey, \
  userIdMapKey, \
  userKey, \
  channelUserKey, \
  joinStatusKey, \
  \
  channelId, \
  userName = \
  \
  KEYS[1], \
  KEYS[2], \
  KEYS[3], \
  KEYS[4], \
  KEYS[5], \
  \
  ARGV[1], \
  ARGV[2] \
 \
local res = { \
  ['channel'] = redis.call('hget', channelKey, channelId), \
  ['joinStatus'] = redis.call('hget', joinStatusKey, channelId) \
} \
 \
local userId = redis.call('hget', userIdMapKey, userName) \
if userId then \
  res['user'] = redis.call('hget', userKey, userId) \
  res['channelUser'] = redis.call('hget', channelUserKey, userId) \
end \
 \
return cjson.encode(res)";

scripts.get_Channel_User_fromNameId =
  "local \
    channelIdMapKey, \
    channelKey, \
    userKey, \
    joinStatusKey, \
    \
    channelName, \
    userId = \
    \
    KEYS[1], \
    KEYS[2], \
    KEYS[3], \
    KEYS[4], \
    \
    ARGV[1], \
    ARGV[2] \
  \
  local res = { \
    ['user'] = redis.call('hget', userKey, userId) \
  } \
  \
  local channelId = redis.call('hget', channelIdMapKey, channelName) \
  if channelId then \
    res['channel'] = redis.call('hget', channelKey, channelId) \
    res['joinStatus'] = redis.call('hget', joinStatusKey, channelId) \
  end \
  \
  return cjson.encode(res)";

scripts.get_Channel_User_fromNameName =
"local \
  channelIdMapKey, \
  channelKey, \
  userIdMapKey, \
  userKey, \
  joinStatusKey, \
  \
  channelName, \
  userName = \
  \
  KEYS[1], \
  KEYS[2], \
  KEYS[3], \
  KEYS[4], \
  KEYS[5], \
  \
  ARGV[1], \
  ARGV[2] \
\
local res = {}  \
\
local channelId = redis.call('hget', channelIdMapKey, channelName) \
if channelId then \
  res['channel'] = redis.call('hget', channelKey, channelId) \
  res['joinStatus'] = redis.call('hget', joinStatusKey, channelId) \
end \
\
local userId = redis.call('hget', userIdMapKey, userName) \
if userId then \
  res['user'] = redis.call('hget', userKey, userId) \
end \
\
return cjson.encode(res)";
