"use strict";

const
  _ = require('lodash'),
  async = require('async'),

  conf = require('thebot-conf'),
  log = conf.log(),
  redis = conf.redis(),
  redisPub = conf.redis('commandPublisher'),
  joinQueue = require('thebot-joinqueue')(redis),
  redisScripts = require('./redis-scripts'),
  PluginManager = require('./plugin-manager'),
  db = require('thebot-db')(),
  TwitchResolver = db.TwitchResolver,
  Channel = db.Channel,
  User = db.User,
  ChannelUser = db.ChannelUser,
  ChannelPolicies = require('./channel-policies');
  
module.exports = CommandService;

function CommandService(pluginManager){
  this.pluginManager = pluginManager;

  let resolver = new TwitchResolver();
  this.resolvers = {};
  this.resolvers[resolver.name] = resolver;
  this.channelPolicies = new ChannelPolicies(conf);
}

let ObjectId = db.db.Types.ObjectId;

/**
 * Tries to execute the given command request.
 * @param cmd
 * @param _callback
 */
CommandService.prototype.execCommand = function(cmd, _callback){
  let callback = this.commandExecuted.bind(this, cmd, _callback);

  if(!this.validateCommandArg(cmd)){
    callback(new Error('BOT_CMD_INVALID'));
    return;
  }

  cmd.commandService = this;
  cmd.log = log.child({ creq: cmd });

  let
    channelId = cmd.channelId,
    userId = cmd.userId,
    resolver;

  if(!channelId || !userId){
    cmd.resolver = resolver = this.resolvers[cmd.channelService];

    if(!resolver){
      callback(new Error('unknown service'));
      return;
    }

    if(!channelId){
      cmd.channelId = channelId = resolver.getChannelId(cmd.channelName);
    }

    if(!userId){
      cmd.userId = userId = resolver.getUserId(cmd.userName);
    }
  }

  let cb = this.fillMissingCommandArgs.bind(this, cmd, callback);

  // TODO acquire channel lock
  //cmd.locked = true;

  /* jshint evil:true*/
  if(channelId){
    if(userId){
      redis.eval(redisScripts.get_Channel_User_ChannelUser_fromIdId,
        4,
          Channel.getCacheKey(),
          User.getCacheKey(),
          ChannelUser.getCacheKey(channelId),
          joinQueue.getJoinStatusKey(),
        channelId.id,
        userId.id,
        cb
      );
    } else {
      redis.eval(redisScripts.get_Channel_User_ChannelUser_fromIdName,
        5,
          Channel.getCacheKey(),
          resolver.getUserMapCacheKey(),
          User.getCacheKey(),
          ChannelUser.getCacheKey(channelId),
          joinQueue.getJoinStatusKey(),
        channelId.id,
        cmd.userName,
        cb
      );
    }
  } else if(userId){
    redis.eval(redisScripts.get_Channel_User_fromNameId,
      4,
        resolver.getChannelMapCacheKey(),
        Channel.getCacheKey(),
        User.getCacheKey(),
        joinQueue.getJoinStatusKey(),
      cmd.channelName,
      userId.id,
      cb
    );

  } else {
    redis.eval(redisScripts.get_Channel_User_fromNameName,
      5,
        resolver.getChannelMapCacheKey(),
        Channel.getCacheKey(),
        resolver.getUserMapCacheKey(),
        User.getCacheKey(),
        joinQueue.getJoinStatusKey(),
      cmd.channelName,
      cmd.userName,
      cb
    );
  }
};

/**
 * Checks if the given command descriptor has a valid format and that the requested command id exist.
 * @private
 * @param cmd
 * @returns {boolean}
 */
CommandService.prototype.validateCommandArg = function(cmd){
  if(!(cmd && cmd.id && _.isString(cmd.id) && this.pluginManager.isKnownCommand(cmd.id))){
    return false;
  }

  if(cmd.channelName){
    if(!(cmd.channelName && _.isString(cmd.channelName) && cmd.channelName.length < 32 && cmd.channelService)){
      return false;
    }
  } else {
    if(!(cmd.channelId && (cmd.channelId instanceof ObjectId || ObjectId.isValid(cmd.channelId)))){
      return false;
    }
    cmd.channelId = new ObjectId(cmd.channelId);
  }

  if(cmd.userName){
    if(!(cmd.userName && _.isString(cmd.userName) && cmd.userName.length < 32 && cmd.channelService)){
      return false;
    }
  } else {
    if(!(cmd.userId && (cmd.userId instanceof ObjectId || ObjectId.isValid(cmd.userId)))){
      return false;
    }
    cmd.userId = new ObjectId(cmd.userId);
  }

  return true;
};

/**
 * Tries to find and load the missing command context arguments that are required for the proper
 * execution of the command.
 *
 * The missing arguments are those who couldn't be loaded directly from the redis cache, for most of them
 * we just reloaded them for the database and after the command has executed, inserted into redis.
 *
 * User and ChannelUser entities will be added to the database if not present, however channels must be present
 * into the database or an error is returned. Channel entities will be "loaded only" or "loaded or created"
 * based on the value of the config flag "channel.createIfMissing"
 *
 * After all dependencies are loaded, the command is executed.
 *
 * @private
 * @param cmd The command request
 * @param callback The original command callback
 * @param {?Error} err The error returned by redis
 * @param {string} res The results returned by redis
 */
CommandService.prototype.fillMissingCommandArgs = function(cmd, callback, err, res){
  if(err){
    callback(err);
    return;
  }

  res = JSON.parse(res);

  let
    self = this,
    tasks = {},
    genericArgs = {
      resolver: cmd.resolver
    },
    channelArgs = genericArgs,
    userArgs = genericArgs,
    channelUserArgs = _.clone(genericArgs);

  // The channel can be a JSON string, null or undefined
  if(res.channel){
    // The channel was successfully loaded, quickly restore it in memory
    tasks.channel = function(cb){
      Channel.parseAndRestoreFromCache(res.channel, channelArgs, cb);
    };
  } else {
    // We tried to get a channel from the specified name or channelId, however the channel wasn't in the cache
    // this means that either a) the channel hasn't been loaded into the cache yet or, n) the channel was removed
    // from the cache for some reason. Since it isn't in the cache, we load it directly from the database
    if(res.channel === false){
      tasks.channel = function(cb){
        async.waterfall([
          async.apply(self.channelPolicies.getChannel, cmd.channelId || cmd.channelName, channelArgs),
          self.channelSetUpFromDatabaseCallback.bind(self)
        ], cb);
      };
    } else {
      // If its undefined it means we tried (unsuccessfully) to map channelName to valid channelId.
      // This happens if the channel hasn't been loaded into the cache yet, or if the channelName to channelId
      // hash was somehow altered. In any case, we must try to check the cache again once we acquire the real
      // channelId, because if we just load directly from the database, we might overwrite an already existing
      // cache entry, along its state.
      tasks.channel = function(cb){
        async.waterfall([
          async.apply(self.channelPolicies.getDatabaseChannel, cmd.channelName, channelArgs),

          function(dbChannel, cb){
            if(dbChannel){
              Channel.loadFromCache(redis, dbChannel._id, channelArgs, function(err, channel){
                if(err){
                  cb(err);
                } else if(channel){
                  cb(null, channel);
                } else {
                  Channel.setUpFromDatabase(dbChannel, channelArgs, function(err, channel){
                    if(err){
                      cb(err);
                    } else {
                      self.channelSetUpFromDatabaseCallback(channel, cb);
                    }
                  });
                }
              });
            } else {
              cb(new Error('unknown channel'));
            }
          }
        ], cb);
      };

      // We also update the channelName to channelId map so other instances of the core find it later on.
      // Do this detached from the request because its unimportant.
      tasks.updateChannelNameMap = ['channel', function (cb, results) {
        redis.hset(cmd.resolver.getChannelMapCacheKey(), cmd.channelName, results.channel.id.id, function(err){
          if(err){
            cmd.log.warn(err, 'Could not update channel-to-id map cache key "%s"',
              cmd.resolver.getChannelMapCacheKey());
          }
        });
        cb();
      }];
    }
  }

  // Set the command's channel id if it wasn't set the at the start
  if(!cmd.channelId){
    tasks.setCommandChannelId = ['channel', function(cb, results){
      cmd.channelId = results.channel.id;
      cmd.resolver.setChannelId(cmd.channelName, cmd.channelId);
      cb();
    }];
  }

  if (res.joinStatus || res.joinStatus === false) {
    // The join status is set or they key wanst found (i.e. its false), which means is not online
    tasks.setJoinStatus = ['channel', function (cb, results) {
      self.setChannelState(results.channel, res.joinStatus);
      cb();
    }];
  } else {
    // If the joinStatus wasn't set, the channelName to channelId map didn't work, so wait till the channel
    // is loaded and try get the status again
    tasks.setJoinStatus = ['channel', function (cb, results) {
      let ch = results.channel;
      async.waterfall([
        async.apply(joinQueue.getJoinStatus, ch.id.id),
        function(joinStatus, cb){
          self.setChannelState(ch, joinStatus);
          cb();
        }
      ], cb);
    }];
  }

  if(res.user){
    tasks.user = function(cb){
      User.parseAndRestoreFromCache(res.user, userArgs, cb);
    };
  } else {

    if(res.user === false){
      tasks.user = function(cb){
        User.findInDatabaseOrCreateAndSetUp(cmd.userId || cmd.userName, userArgs, userArgs, cb);
      };
    } else {
      tasks.user = function(cb){
        async.waterfall([
          User.findInDatabaseOrCreate.bind(User, cmd.userName, userArgs, userArgs),

          function(dbUser, cb){
            User.loadFromCache(redis, dbUser._id, userArgs, function(err, user){
              if(err){
                cb(err);
              } else if(user){
                cb(null, user);
              } else {
                User.setUpFromDatabase(dbUser, userArgs, cb);
              }
            });
          }
        ], cb);
      };

      tasks.updateUserNameMap = ['user', function (cb, results) {
        redis.hset(cmd.resolver.getUserMapCacheKey(), cmd.userName, results.user.id.id, function(err){
          if(err){
            cmd.log.warn(err, 'Could not update user-to-id map cache key "%s"', cmd.resolver.getUserMapCacheKey());
          }
        });
        cb();
      }];
    }
  }

  // Set the command's user id if it wasn't set the at the start
  if(!cmd.userId){
    tasks.setCommandUserId = ['user', function(cb, results){
      cmd.userId = results.user.id;
      cmd.resolver.setUserId(cmd.userName, cmd.userId);
      cb();
    }];
  }

  if(res.channelUser){
    tasks.channelUser = ['channel', 'user', function(cb, results){
      ChannelUser.parseAndRestoreFromCache(res.channelUser, _.extend(channelUserArgs, results), cb);
    }];
  } else {

    if(res.channelUser === false){
      tasks.channelUser = ['channel', 'user', function(cb, results){
        _.extend(channelUserArgs, results);

        ChannelUser.findInDatabaseOrCreateAndSetUp({
          user: results.user.id,
          channel: results.channel.id
        }, channelUserArgs, channelUserArgs, cb);
      }];
    } else {
      tasks.channelUser = ['channel', 'user', function(cb, results){
        _.extend(channelUserArgs, results);

        ChannelUser.restoreOrCreate(redis, {
          user: results.user.id,
          channel: results.channel.id
        }, channelUserArgs, channelUserArgs, cb);
      }];
    }
  }

  // Execute the command when all requirements are met
  async.auto(tasks, function(err, results){
    if(err){
      callback(err);
    } else {
      cmd.channel = results.channel;
      cmd.user = results.user;
      cmd.channelUser = results.channelUser;
      self.doExecuteCommand(cmd, callback);
    }
  });
};

/**
 * @private
 * @param channel
 * @param callback
 */
CommandService.prototype.channelSetUpFromDatabaseCallback = function(channel, callback){
  if(channel){
    this.pluginManager.setUpCommands(channel.dbEntity, channel, function(err){
      callback(err, channel);
    });
  } else {
    callback(new Error('unknown channel'));
  }
};

/**
 * @private
 * @param channel
 * @param joinStatus
 */
CommandService.prototype.setChannelState = function(channel, joinStatus){
  // TODO we have to later put this on 'paused' somehow
  channel.state = joinStatus === 'joined' || joinStatus === 'parting'? 'online' : 'offline';
};

/**
 * Executes the given command description.
 * @private
 * @param cmdOpts
 * @param callback
 */
CommandService.prototype.doExecuteCommand = function(cmdOpts, callback){
  let cmd = this.pluginManager.getCommand(cmdOpts.id, cmdOpts.channel);

  if(typeof cmd === 'object'){
    cmd.exec(cmdOpts.channel, cmdOpts, callback);
  } else {
    switch(cmd){
      case PluginManager.CMD_INVALID_STATE:
        callback(new Error('BOT_CMD_NOT_AVAILABLE'));
        break;

      // The command (and it's plugin) is new to the channel and hasn't been loaded into it yet, this can happen if
      // a channel has newly acquired plugin permission or if there is a new global plugin who was just loaded into
      // the core. In any case, load it and try again.
      case PluginManager.CMD_NOT_LOADED:
      {
        let self = this;
        this.pluginManager.setUpCommand(cmdOpts.id, cmdOpts.channel, function(err) {
          if (err) {
            callback(err);
          } else {
            self.doExecuteCommand(cmdOpts, callback);
          }
        });
        break;
      }
    }
  }
};

/**
 * Returns whether the given command trigger is in use by the channel.
 * This also takes into account temporary commands added to the channel via addCommands directives.
 * Note that all loaded commands' triggers (including disabled ones) are considered to be in use, whether or not they
 * are active on the channel.This is to prevent any conflicts to emerge when one of these commands gets
 * activated on the channel.
 */
CommandService.prototype.commandTriggerExists = function(channel, trigger){
  // Check all command ids first since this is a constant operation
  if(this.pluginManager.isKnownCommand(trigger)){
    return true;
  }

  // Check temporary commands as there are just few of them
  let exists = false;
  channel.tempCommands.forEach(checkExistence);

  if(exists){
    return true;
  }

  // Otherwise check all commands' triggers one by one
  _.forOwn(this.pluginManager.plugins, function(plugin){
    plugin.getCommandTriggers().forEach(checkExistence);
  });

  if(exists){
    return true;
  }

  function checkExistence(cmd){
    if(cmd.id === trigger || (cmd.aliases && _.contains(cmd.aliases, trigger))){
      exists = true;
      return false;
    }
  }

  return false;
};

/**
 * Executes the given command descriptor, without committing ant Channel, User or ChannelUser dirty state into redis.
 * This is useful for commands that need to execute other commands in the same request, i.e. for the same user and
 * channel. The channel, user and channelUser keys must be present in the given command descriptor.
 * @param cmdOpts
 * @param callback
 */
CommandService.prototype.execInternal = function(cmdOpts, callback){
  cmdOpts.source = 'internal';
  cmdOpts.internal = true;
  this.doExecuteCommand(cmdOpts, callback);
};

CommandService.prototype.scheduleCommand = function(cmdOpts, time, callback){
  let self = this;
  setTimeout(function(){
    self.execCommand(cmdOpts, callback || _.noop);
  }, time).unref();
};

/**
 * @see {PluginManager#getCommandTriggers}
 * @param channel
 * @param callback
 */
CommandService.prototype.getCommandTriggers = function(channel, callback){
  this.pluginManager.getCommandTriggers(channel, callback);
};

/**
 * Runs after every command execution.
 * This function makes sure to save dirty Channel, User and ChannelUser state back into redis, as well as publishing
 * any results such as channel messages and feedback to their respective destinations.
 *
 * @private
 * @param cmd
 * @param callback
 * @param err
 * @param res
 */
CommandService.prototype.commandExecuted = function(cmd, callback, err, res){
  // TODO release channel lock

  if(err){
    callback(err, res);
    return;
  }

  // Check for possible changes
  if(!cmd.skipCache){
    cmd.channel.cacheChanges();
    cmd.user.cacheChanges();
    cmd.channelUser.cacheChanges();
  }

  // Output
  if(res && !res.skipResponse){
    let
      key = joinQueue.getChannelMessagesKey(cmd.channelId),
      cb = this.publishResponseCallback.bind(this, cmd);
    
    if(res.output){
      redisPub.publish(key, JSON.stringify({
        type: 'write',
        output: res.output
      }), cb);
    }
    
    if(res.addCommands){
      redisPub.publish(key, JSON.stringify({
        type: 'addCommands',
        commands: res.addCommands
      }), cb);
    }
    
    if(res.delCommands){
      redisPub.publish(key, JSON.stringify({
        type: 'delCommands',
        commands: res.delCommands
      }), cb);
    }
    
    if(res.allCommands){
      redisPub.publish(key, JSON.stringify({
        type: 'allCommands',
        commands: res.allCommands
      }), cb);
    }

    if(res.part){
      redisPub.publish(key, JSON.stringify({
        type: 'part'
      }), cb);
    }
  }
    
  callback(null, res);
};

/**
 * Utility callback wrapper for publishing responses.
 * @private
 * @param cmd
 * @param err
 * @param clients
 */
CommandService.prototype.publishResponseCallback = function(cmd, err, clients){
  if(err){
    cmd.log.warn(err, 'Unable to publish response');
  } else {
    cmd.log.trace('Response published to %d clients.', clients);
  }
};