"use strict";

const
  _ = require('lodash'),
  async = require('async'),
  
  conf = require('thebot-conf'),
  redis = conf.redis(),
  Channel = require('thebot-db')().Channel;

module.exports = PluginCommandWrapper;

/**
 * Holds a plugin and its commands in a single executable object.
 * This class is used by the pluginCompiler function as the default wrapper for the generated plugin.
 * @constructor
 */
function PluginCommandWrapper(){}

// Plugin related operations

/**
 * Sets up required plugin data for a newly loaded channel from the database.
 * We return the settings so that they can be aggregated and updated in one operation inside redis.
 */
PluginCommandWrapper.prototype.setUp = function(dbChannel, chData){
  let settings = {};
  settings[this.pluginName] = JSON.stringify(this.loadPluginSettings(dbChannel, chData));
  
  return settings;
};

/**
 * Loads required plugin data from the specified db.Channel and puts in on the given Channel.
 */
PluginCommandWrapper.prototype.loadPluginSettings = function(dbChannel, ch){
  ch.loadedPlugins.push(this.pluginName);

  //Set the starting plugin state so the channel can retrieve the correct command to execute during start
  if(this.startingState){
    ch.pluginState[this.pluginName] = this.startingState;
  }

  let pState = ch.tempStore[this.pluginName] = {};
  
  //Each command can have different settings configured, we load them and pass them up
  // so they can be inserted in redis in one op
  _.forOwn(this.commands, function(cmdMap, cmdId){
    pState[cmdId] = this.getRawCommand(cmdId).loadCommandSettings(dbChannel);
  }, this);
  
  return pState;
};

/**
 * Sets up required plugin data for an existing channel already loaded in redis.
 */
PluginCommandWrapper.prototype.setUpOnLoaded = function(ch, callback){
  let self = this;

  async.waterfall([
    Channel.findInDatabase.bind(Channel, ch.id, null),

    function(dbChannel, cb){
      self.loadPluginSettings(dbChannel, ch);
      ch.loadedPlugins.sort();
      ch.dirty();
      self.saveStored(ch, cb);
    }
  ], callback);
};

PluginCommandWrapper.prototype.getRawCommand = function(cmdId){
  for(let cmdState in this.commands[cmdId]){
    if(this.commands[cmdId].hasOwnProperty(cmdState)){
      return this.commands[cmdId][cmdState];
    }
  }
};

PluginCommandWrapper.prototype.getCommandTriggers = function(){
  let triggers = [];
  _.forOwn(this.commands, function(cmdMap, cmdId){
    let t = this.getRawCommand(cmdId);
    
    if(t.triggers){
      triggers.push.apply(triggers, t.triggers);
    }
  }, this);
  
  return triggers;
};


// Command related operations

// TODO Actually use the db info..
PluginCommandWrapper.prototype.loadCommandSettings = function(dbChannel){
  return {
    disabled: false
  };
};


/**
 * Loads the stored plugin state.
 * Since all command state is initially loaded via PluginCommandWrapper#setUp or after the channel has been loaded
 * via CommandService#doExecuteCommand, the stored state is guaranteed to be already present, however, in some
 * circumstances it may not be, and without that state the command cannot run.
 *
 * We need to set up the plugin in the channel manually, because otherwise we would fail to check the
 * command's auth level and other basic preconditions.
 */
PluginCommandWrapper.prototype.loadStored = function(ch, callback){
  if(ch.tempStore[this.pluginName]){
    callback(null, ch.tempStore[this.pluginName]);
  } else {
    let self = this;
    async.waterfall([
      redis.hget.bind(redis, self.cacheKey(ch), self.pluginName),
      function(data, cb){
        if(data){
          cb(null, ch.tempStore[self.pluginName] = JSON.parse(data));
        } else {
          async.waterfall([
            self.setUpOnLoaded.bind(self, ch),
            function(cb){
              cb(null, ch.tempStore[self.pluginName]);
            }
          ], cb);
        }
      }
    ], callback);
  }
};

PluginCommandWrapper.prototype.getStored = function(ch){
  return ch.tempStore[this.pluginName];
};

/**
 * Removes any non-command stored information from the plugin store.
 */
PluginCommandWrapper.prototype.clearStored = function(ch){
  let
    actual = ch.tempStore[this.pluginName],
    cleared = {};
  
  _.forOwn(this.commands, function(cmd, cmdId){
    cleared[cmdId] = actual[cmdId];
  });
  
  ch.tempStore[this.pluginName] = cleared;
  return this.getStored(ch);
};

PluginCommandWrapper.prototype.saveStored = function(ch, callback){
  if(!ch.tempStore[this.pluginName]){
    callback(new Error('no channel data to save'));
  }
    
  redis.hset(this.cacheKey(ch), this.pluginName, JSON.stringify(ch.tempStore[this.pluginName]), function(err/*, arg*/){
    callback(err);
  });
};

PluginCommandWrapper.prototype.cacheKey = function(ch){
  return ch.getCommandsKey();
};

PluginCommandWrapper.prototype.getPluginState = function(ch){
  return ch.pluginState[this.pluginName];
};

PluginCommandWrapper.prototype.setPluginState = function(ch, state) {
  ch.pluginState[this.pluginName] = state;
  ch.dirty();
};

PluginCommandWrapper.prototype.exec = function(ch, opts, _callback){
  if(!this.validateArgs(opts)){
    _callback(new Error('invalid arguments'));
    return;
  }
  
  let self = this,
    neededData = {};
    
  neededData.stored = function(callback){
    self.loadStored(ch, function(err){ callback(err); });
  };

  async.parallel(neededData, function(err, res){
    async.waterfall([
      function(callback){
        self.validate(ch, opts, function(err, valid, res){
          callback(err || (valid? null : new Error('BOT_CMD_NOT_AUTHORIZED')), res);
        });
      },
      function(res, callback){
        self.before(ch, opts, callback || res, callback? res : null);
      },
      function(res, callback){
        self._exec(ch, opts, callback || res, callback? res : null);
      },
      function(res, callback){
        if(_.isPlainObject(res)){
          res.req = opts;
          self.packResponse(ch, res);
        }
        
        self.after(ch, opts, callback, res);
      }
    ], _callback);
  });
};

// FIXME We need this ASAP, probably just use ircio/command-parser.js again to validate this
PluginCommandWrapper.prototype.validateArgs = function(opts){
  return true;
};

PluginCommandWrapper.prototype.validate = function(ch, opts, callback){
  let
    data = this.getStored(ch)[this.id],
    groups = data.authGroups || this.authGroups,
    auth = data.auth || this.auth,
    channelUser = opts.channelUser,
    ok = false;
  
  if(auth){
    ok = channelUser.isAnyOf(auth) || (groups && channelUser.hasAnyCustomGroup(groups));
  } else{
    ok = !groups || channelUser.hasAnyCustomGroup(groups);
  }

  // FIXME you should be ashamed
  callback(null, ok || channelUser.isOwner() ||
    _.contains(['netotigr', 'zyntaxterror', 'gr8team', 'natedogg1429'], channelUser.nick));
};

PluginCommandWrapper.prototype.before = function(ch, opts, callback, res){
  callback(null, res);
};

// TODO Apply callback
PluginCommandWrapper.prototype.packResponse = function(ch, res, callback){
  if(_.isPlainObject(res.output)){
    res.output = this.messages[res.output.message].replace(/(?:(\\)(\$|#))|(?:(\$|#)(!?)(\w+))/g,
    function(match, isScaping, symbol, type, justFormat, varName){
      if(isScaping){
        return symbol;
      }
      
      switch(type){
      case '$':
        return res.output[varName];
      case '#':
        return '#' + (justFormat? varName : res.output[varName]);
      }
    });
    
    res.output = res.output
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
  }
  
  /*if(res.feedback){
    
  }*/
  
  if(res.addCommands){
    ch.tempCommands.push.apply(ch.tempCommands, res.addCommands);
    ch.dirty();
  }
  
  if(res.delCommands){
    ch.tempCommands = _.reject(ch.tempCommands, function(cmd){
      return res.delCommands.indexOf(cmd.id) !== -1;
    });
    ch.dirty();
  }
  
  if(callback){
    callback(null);
  }
};

PluginCommandWrapper.prototype.after = function(ch, opts, callback, res){
  callback(null, res);
};

/**
 * Creates a new command request object with the values extracted from an already constructed one.
 * @param baseCmd
 * @param {?Object} extraOpts
 */
PluginCommandWrapper.prototype.createCommand = function(baseCmd, extraOpts){
  return _.extend(extraOpts || {}, _.pick(baseCmd, [
    'id',
    'channelService',
    'source'
  ]), {
    'channelId': baseCmd.channel.id,
    'userId': baseCmd.user.id
  });
};