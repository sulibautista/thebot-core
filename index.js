"use strict";

const
  conf = require('thebot-conf'),
  log = conf.log('default', { name: 'thebot-core' }),
  PluginManager = require('./lib/plugin-manager.js'),
  CommandService = require('./lib/command-service.js'),
  SioEndpoint = require('./lib/socket.io-endpoint.js');

let pluginManager = new PluginManager();

pluginManager.loadPlugins(__dirname + '/lib/commands', function(err){
  if(err){
    throw err;
  }
  
  let cmdSvc = new CommandService(pluginManager);

  let sioEndpoint = new SioEndpoint({
    commandService: cmdSvc,
    port: process.env.PORT || 80
  });

  // Allow external connections via socket io
  sioEndpoint.start();
  
  setInterval(function(){
    cmdSvc.execCommand({
      id: 'join',
      userName: conf.get('startingChannel'),
      channelName: conf.get('startingChannel'),
      channelService: 'twitch'
    }, function(err, res){
      if(err && err.message !== 'missing channelArg argument'){
        log.error(err, 'Error joining init channel');
      }
    });
  }, 10000);
});
