Processor
  Main loop should be abstracted from network interfaces
    -> Allows us to receive input from different sources/interfaces
    -> Quick testing in standalone apps
    
  This is the principal architectural component, therefore if it goes down all other services are expected to be interrupted, in any case we should strive for a "hotswap"-like architecture, where other components stay in place waiting for us after we banish.
    -> IRC Bots can stay forever in channels even if the main app crashes or needs maintenance
    -> We must ensure that all state gets correctly synced between the app and redis/mongo
    
  Shared state
    -> Inner channel state, redis sync, see channel
    -> Channel join state, heartbeat during initialization, see IRC Bots
    
Commands
  Commands are/have
    -> Finite, all the commands that are supported are pre-programed inside all the tools. We may need to develope a technice for custom command request, we may want to hot-insert those on the fly
    -> Identified uniquely by a command id, no matter what the actual command in the chat is, the command always has an internal id.
    -> Costumizable output, every client can customize the messages the bot sends to the chat, and most likely those who display on the web site are influenced by some small strings (like "points")
    -> Permissions, in the form of "user is: "
          * Following the stream
          * Subscribed to the stream ($$)
          * Belongs to a user-defined role (like "Regular")
          * Is moderator
          
       Stream owner has access to all the commands. Valid combs:
       
          * Everyone
          * Following & Subscribed & Mods
          * Following & Subscribed
          * Subscribed & Mods
          * Subscribed
          * Mods
          * Just me
          
          + Extra groups
  
    -> Different configs for different games
      -> Output/String changes
      -> Command trigger changes
      -> Different default params
      
    -> Cmd output as short as possible
          
          
    
Channel
  We use redis for everything.
  
IRC Bots
  IRC bots have two functions, first, the listen to messages on the channels they are managing, parsing them to actual commands and sending over to the main processor. They also listen for messages to write on theyre respective channels.
  
  We dont need:
    -> To keep channel write messages on a persistent queue, we dont care if we drop some mensages, because most of the time messages are outdated after a few seconds/minutes
    -> To keep entrant command messages on a persistent queue, for the same reasons as above.
    
  We need:
    -> To persist other commands like 'join' on a separate queue, since these are a common bottleneck and we can't afford to drop these messages (for commercial reasons)
    -> To heartbeat bots in order to detect crashes or other malfunctioning, as soon as we detect a bot if offline, we must requeue joins ASAP

  We could use:
    -> PUSH / PULL (with no intermediate queue), for the write-like commands
    -> Redis Queue, for the join-like commands
    -> PUB / SUB / PUSH, in the form of processor -> bots, for the hertbeat (pub ping on processor, and push back pong)


Clients
  We need to support two types of clients:
  -> IRC Clients
    * They care about two things, state and output, from our point of view, theyre basically fire and forget. 
    
  -> Web Clients
    * For these clients, feedback is needed, aside from state and channel output.
    
  Examples:
    join
      irc: Implicit connection within private network, add channel to join queue, end.
      web: Explicit connection from a web browser, add channel to join queue, when its done, notify UI.
  
  As we can see not only the requirements for the behaviour are different, but the connection method aswell. Another important point to consider is the fact that multiple web clients can be "connected" to the same channel, and they both should receive feedback.
      
  In a closer look, if we initiate and action via irc, we should be able to get notifications from the web. So maybe there is a fixed element in here, the feedback system.
   
* Users
  Chat users
    -> Users are per channel, we dont maintain state across channels, but we should be able to present a 'dashboard' for the user in the future
    -> 
    
  Clients

~~~~~~~~~~~~~~~~~~~~~ WORKING ~~~~~~~~~~~~~~~~~~~~~

* On command config update, we hit redis, re-set all config related keys and leave state untouched, like a "partial setup"

* When do users get loaded?
  -> before validation
  -> validation requires channel config + user privilegies so we should load both before validating
  -> are users cached into redis? 
    -> we could cache them with a very low ttl, like 10m?
    -> what do we cache anyway?
      -> permissions & groups ? seems like the only thing we can cache, 
      -> command interval? seems legit, we might throttle this 
      -> what would it look like? str user:channel:user_name
      
  -> how do we handle interval point changes?
    -> we use twitch api for retrieving names, apply mongo querys on then points
    -> since we wont cache points, do not need to invalidate cache
    


* Network structure
    -> zset joinQueue {channel priority} - channel join queue
    -> zset channelWriters {uuid: timestamp} - activity register for detecting dead handlers
    -> set channelWriter:uuid:channels - list of channels handled by the channel writer
    
    
    Join Algorithm
      Whenever twitch allow us and we havent reached max channel count <- This is flawed, we want to distribute the load evenly See below
      Execute atomically:
        Get channelWriters <uui> // to see if we are still connected
        If not
          return 'disconnected'
          
        Take one channel from joinQueue
        If channel
          Fetch joinState channel
          If !joinState 
            Add channel to channelWriter:<uuid> set
            Set joinState = 'joining'
            Return channel
          Else
            return nil
        Else
          return nil
      If disconnected
        flush channels and restart
      Else if channel
        connect to twitch
      Else
        ignore and retry later
      ...
      When twitch actually joins us..
      Set channel:<channelid>:join state = 'joined'
      
      #Flaws
      We might need another application that load balances the work to each worker, because different channels use resources in varying amounts. So we should choose the lesss loaded worker of all of them to process a join. We also need recover mechanisms in the following cases:
        * Channel load (outgoing msgs/unit time) is not sustainable in the current worker (because of twitch limitations). We might need
          to "pass" the channel to a different less-loaded worker or even launch a new one.
        * Somehow we broke twitch msgs/unit-time limit, and we get banned for 8 hours. This maybe for one of 2 reasons:
          -> Software error. We fuck'd up, so we retry and investigate on logs.
          -> Twitch fuckd us. Twitch is known for changing shit in the middle of the night, if we suspect this is the reason we might need to alert admins and shutdown.
      
    Keep alive
      Every 1s
      Execute atomically:
        Get channelWriters <uui>
        If found
          Set channelWriters <uuid> = Date.now()
          return TRUE
        Else
          return False
      If true
        continue as normal
      Else
        we have been disconnected, flush all channel connections and restart
      
    Channel Keep alive checker
      Every 1s
      Execute atomically:
        Get channelWritters where timestamp < (time - 3s)
        Result = []
        ForEach <uuid>s 
          Delete channelWritter <uuid>
          Get & Delete channelWriter:<uuid>:channels
          ForEach channels
            Delete channel:<channelid>:joinState
            // In case keep aliver crashes, the commands get rejoined, later we update the priority index
            PUSH joinQueue <channelid> channel LOWERT PRIORITY + Date.now() 
            Result += channel
        Return Result
      If result
        ForEach channel
          Get channel:<channelid> or from db
          PUSH joinQueue <channelid> channel join priority + Date.now()
        
    Single channel disconnect from twitch
      > is this even possible?
        > maybe with a group of channels, but one is not likely
        > how do we invalidate those channels thne?
        > MISSED: Intentionnal parts!
        
      Execute atomically
        ForEach channel
          Delete channel:<channelid>:joinState
          PUSH joinQueue <channelid> channel LOWERT PRIORITY + Date.now()
          Delete channelWriter:<uuid>:channel <channelid>
      ForEach channel
        Get channel:<channelid> or from db
        PUSH joinQueue <channelid> channel join priority + Date.now()
    
    Check join state from app
      Get channel:<channelid>:joinState
      
      > Since we dont modify this value we dont actually care too much about its accuracy, commands
        that deppends on sending data will try and data will just get ignored
      
    Enqueue join from app
      Execute atomically:
        Get joinQueue <channelid>
        If not exist
          ADD joinQueue <channelid> channel join priority + Date.now()
        
        > What happens if we double enqueue?
          > If we are on high load, the channel will be still in the queue and the push will have no effect
          > If we happen to add one channel just after the same channel was removed from the queue
            -> The first channel joiner will try to join and will succed
          -> Subsequent channel joiners will see 'joining' or 'joined' and perform no action
    

    Write to chnanels
      > ZMQ Pub/Sub
        Publish 'channel:<id>'
          Messages
          Parts
          Other stuff
        Suscribe 'channel:input'
          Incoming messages
          
        We don't care about message persistance, so using lossy is appropiate her
        
      > Redis Pub/Sub
        Conceptually the same, but we use only another connection to redis and no more socckets, which could cause performance issues
        
      > What happens to PART (leave chanel) messages?
        They get push, if we crash while sending, it does nothing, user has to retry
        If we crash on the receiving end, we actually parted
        
   Parting algo
      Start by removing the channel from channelHandlers:<uuid>, so even if we crash it doesnt get auto rejoined by keep alive
      We also set an expire on the channel status key of like 10s, bcuz if we crash the channel might be 'online forever' and
      change its state to 'parting'
      Enqueue the part message with top priority internally
      While it still not sended, update the expire every 5s, to 10s, this gurantees that other handlers dont try to join while we are
      parting, if we crash during this period, we effectively parted, and the key expire will kick in allowing other handlers to rejoin
      if required
      Once we send it and twitch disconnected us, stop refreshing expire and let go of any resources associated with the channel
        
        
      > How does client knows state?
        > Channel workers can report back to mainframe with status messages, when actually joining or parting
          If the sender crashes those messages wont be send, but the actual channel "state" will be on redis,
          so clients can poll them easily, 
        
        > So if everything runs fine, clients get immediate feedback on channel state
        > If channel writers crash, keep aliver restores them and notifies web clients
        > If core crash, web client reconnects and ask for state @start
       
          
        
      